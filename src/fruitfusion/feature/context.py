from fruitfusion.feature import ImageFeatures
from skimage.color.colorconv import rgb2hsv
from fruitfusion.textures import noise_level_estimation
import numpy as np
from fruitfusion.textures.unser import sum_histogram, difference_histogram, mean,\
    variance, energy, correlation, entropy, contrast, homogeneity, cluster_shade,\
    cluster_prominence
from fruitfusion import colours

D = 4

class NoiseFeatures(ImageFeatures):
    
    def __init__(self, load_pattern, target_names, d=D):
        ImageFeatures.__init__(self, load_pattern, target_names)
        self.d = d
    
    def process_image(self, img):
        hsv = rgb2hsv(img)
        hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
        
        features = []
        features.append(noise_level_estimation(val))
        
        h_s = sum_histogram(val, D, D)
        h_d = difference_histogram(val, D, D)
        
        m = mean(h_s)
        features.append(variance(h_s, h_d, m))          # 1
        features.append(energy(h_s, h_d))               # 2
#         features.append(correlation(h_s, h_d, m))       # 3
        features.append(entropy(h_s, h_d))              # 4
        features.append(contrast(h_d))                  # 5
        features.append(homogeneity(h_d))               # 6
#         features.append(cluster_shade(h_s, m))          # 7
#         features.append(cluster_prominence(h_s, m))     # 8
        
        return np.array(features)[:,np.newaxis]

class ColourFeatures(ImageFeatures):
    
    def __init__(self, load_pattern, target_names, d=D):
        ImageFeatures.__init__(self, load_pattern, target_names)
        self.d = d
    
    def process_image(self, img):
        hsv = rgb2hsv(img)
        hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
        chroma = sat*val
        
        features = []
#         features.append(colours.wnukowicz(img))#
#         features.append(colours.wnukowicz(img, thresh=0.01))
#         features.append(colours.wnukowicz(img, thresh=0.1))
#         features.append(colours.wnukowicz(img, f=2))
#         features.append(colours.wnukowicz(img, f=5))
#         features.append(colours.wnukowicz(img, thresh=0.01, f=1))
#         features.append(colours.wnukowicz(img, thresh=0.1, f=1))
#         features.append(colours.wnukowicz(img, thresh=0.01, f=5))
#         features.append(colours.wnukowicz(img, thresh=0.1, f=5))
#         features.append(colours.temperature(hue, sat, val))
#         features.append(colours.white_balance(img))#
        features.extend(colours.perfect_reflector(img).ravel()[:2])
#         features.append(colours.contrast(chroma))
#         features.append(colours.level(chroma))
#         features.append(colours.histogram_entropy(chroma))
           
        for i, channel in enumerate([hue, sat, chroma]):
            h_s = sum_histogram(channel, D, D)
            h_d = difference_histogram(channel, D, D)
            m = mean(h_s)
               
            if i == 0:
                features.append(m)
                features.append(correlation(h_s, h_d, m))                     
            if i != 0:
                features.append(contrast(h_d))      
            features.append(homogeneity(h_d))             
#             features.append(cluster_shade(h_s, m))#
#             features.append(cluster_prominence(h_s, m))#
        
        return np.array(features)[:,np.newaxis]
    
class ContextFeatures(ImageFeatures):    
    def __init__(self, load_pattern, target_names, d=D):
        ImageFeatures.__init__(self, load_pattern, target_names)
        self.d = d
        
            
    def process_image(self, img):
        hsv = rgb2hsv(img)
        hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
        chroma = sat*val
        
        features = []
        
        # Colour
        features.append(colours.temperature(hue, sat, val)) #0
        features.extend(colours.perfect_reflector(img).ravel()[:2]) #1,2
        features.append(colours.contrast(chroma))
        features.append(colours.level(chroma))
        features.append(colours.histogram_entropy(chroma))
        
        # Texture
        features.append(noise_level_estimation(val))
           
        for i, channel in enumerate([hue, sat, val, chroma]):
            h_s = sum_histogram(channel, D, D)
            h_d = difference_histogram(channel, D, D)
            m = mean(h_s)
               
            if i < 2:
                features.append(m) # 7, 10
            if i == 0:
                features.append(correlation(h_s, h_d, m)) # 8
            else:#TODO: get rid of this!
                features.append(homogeneity(h_d)) # 11, 13, 18
            if i == 2:
                features.append(variance(h_s, h_d, m)) # 14
                features.append(energy(h_s, h_d)) # 15
                features.append(entropy(h_s, h_d)) # 16
            features.append(contrast(h_d)) # 9, 12, 17, 19
        
        return np.array(features)[:,np.newaxis]