from skimage.io.collection import ImageCollection

from sklearn.externals import joblib
from fruitfusion import background, N_JOBS
from fruitfusion import colours
from fruitfusion import textures
from skimage.color import rgb2hsv
import itertools
from fruitfusion.tools import cachedproperty, plot_img
from skimage.io import imread
import random
from fruitfusion import shapes
from fruitfusion.textures.unser import sum_histogram, difference_histogram, mean, variance,\
    energy, correlation, entropy, contrast, homogeneity, cluster_shade,\
    cluster_prominence
import numpy as np
from skimage.feature._hog import hog
from skimage import transform

class ImageFeatures(object):
    
    def __init__(self, load_pattern, target_names):
        '''
        Parameters
        ==========
        add_noise : boolean
            randomly add noise to some of the samples
        '''
#        self.target = property(self._target)
        self._target_names = target_names
        self._DESCR = None
        self._filenames = None
        
        # Assert: targets are in the same order as load_pattern
        self.process(load_pattern)
        
    ########################### sklearn.datasets.Bunch ###########################
    
    @property
    def data(self):
        return self._data.T
    
    @data.setter
    def data(self, data):
        pass
    
    @cachedproperty
    def target(self):
        target = []
        target_ids = []
        for t in self._target_names:
            try:
                i = target_ids.index(t)
            except ValueError:
                target_ids.append(t)
                i = target_ids.index(t)
                
            target.append(i)
        return np.array(target)
    
    @property
    def target_names(self):
        return self._target_names
    
    @property
    def DESCR(self):
        return self._DESCR
    
    @property
    def filenames(self):
        return self._filenames
    
    @property
    def images(self):
        return self._images
    
    ########################### Other Methods ###########################
#    def __getstate__(self):
#        return {'data': self.data, 'target_names': self.target_names, 'images': self.images, 'target': self.target}
#    
#    def __setstate__(self, state):
#        self._data = state['data']
#        self._target_names = state['target_names']
#        self._images = state['images']
#        self._target = state['target']
    
    def process_image(self, img):
        pass
                
    def process(self, load_pattern):
        assert not hasattr(self, '_images'), 'self.images should not already be set'
        
        print 'Loading...'
        
        self._images = ImageCollection(load_pattern)
        
        print 'Images Loaded...'
        
        assert load_pattern == '' or self.images.files == load_pattern, 'The order of the files attribute in self.images.files should be the same as the original order'
        
#        data = []
#        for i, image in enumerate(self.images):
#            if i%10 == 0:
#                print 'Processing image %d of %d for %s' %(i, len(self.images), type(self).__name__)
#            img = self.process_image(image)
#            data.append(img)
        
#        for i in self.images:
#            print self.process_image(i)
        
        print 'N IMAGES: ', len(self.images.files)
        
        data = joblib.Parallel(n_jobs=N_JOBS, verbose=2)(joblib.delayed(self)(i) for i in self.images)
#        data = joblib.Parallel(n_jobs=2, pre_dispatch=0, verbose=1)(itertools.imap(self.process_image, self.images))
        
        d = filter(lambda d: d!=None, data)
        
        if len(data) != len(d): print '%d of %d images skipped.' %(len(data) - len(d), len(data))
        
        if len(d) > 0:
            self._data = np.concatenate(d, axis=1)
        else:
            self._data = np.array([])
        
        print 'Asserted...'
        
        assert self.data.shape[0] == len(load_pattern) - (len(data) - len(d)), 'Should contain the same number of feature sets (%d) as input images (%d), (%s)' %(self.data.shape[0], len(load_pattern), str(self.data.shape))
    
    def __call__(self, img):
        processed = self.process_image(img)
        #FIXME: Choose a better fix here...
#         if np.max(processed) ==  np.min(processed): # Why is this here?
#             return None
        return processed 
    
    def save(self, dest):
        joblib.dump(self, dest)
    
    @classmethod
    def load(cls, src):
        return joblib.load(src)
        
class HistogramColourFeatures(ImageFeatures):
    
    def process_image(self, img):
        hsv = rgb2hsv(img)
        hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
        
        regions = background.regions(sat)
        hist, bins = colours.histogram(hue, mask=regions)
        
        return hist
    
class UnsersDescriptorFeatures(ImageFeatures):
    
    def process_image(self, img):
        hsv = rgb2hsv(img)
        hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
        
        s = sum_histogram(val, 2, 2)[:,np.newaxis] #TODO: should this be 2?
        d = difference_histogram(val, 2, 2)[:,np.newaxis]
        
        return np.concatenate((s,d))
        
class LocalBinaryPatternTextureFeatures(ImageFeatures):
    
    def process_image(self, img):
        hsv = rgb2hsv(img)
        hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
        
        regions = background.regions(sat)
        hist, bins = textures.lbp(val, regions)
        return hist
    
class EdgeOrientationAutocorrelogramFeatures(ImageFeatures):
    
    def process_image(self, img):
        hsv = rgb2hsv(img)
        hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
        
        return shapes.edge_orientation_autocorrelogram(val)
    
class HistogramOfOrientedGradientFeatures(ImageFeatures):
    
    def process_image(self, img):
        hsv = rgb2hsv(img)
        hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
        
        if val.shape != (768,1024):
            val = transform.resize(val,(768,1024))
        
        h = hog(val, orientations=8, pixels_per_cell=(16, 16),
                    cells_per_block=(1, 1), visualise=False)
        
        return h[:,np.newaxis]

class UnserContextFeatures(ImageFeatures):
    
    D = 4
    
    def process_image(self, img):
        hsv = rgb2hsv(img)
        hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
        
        satval = sat*val
        
        assert np.any(satval <= 1), 'Combined saturation and value should be less than 1'
        
        h_s = sum_histogram(satval, self.D, self.D) #TODO: should this be 2?
        h_d = difference_histogram(satval, self.D, self.D)
        
        features = []
        m = mean(h_s)
        features.append(m)
        features.append(variance(h_s, h_d, m))
        features.append(energy(h_s, h_d))
        features.append(correlation(h_s, h_d, m))
        features.append(entropy(h_s, h_d))
        features.append(contrast(h_d))
        features.append(homogeneity(h_d))
        features.append(cluster_shade(h_s, m))
        features.append(cluster_prominence(h_s, m))
        
        return np.array(features)[:,np.newaxis]

class ContextFeatures(ImageFeatures):
    
    def process_image(self, img):
        hsv = rgb2hsv(img)
        hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
        chroma = sat*val #FIXME should this be (1-val)?
        
        chroma_fns = [
                   colours.contrast,
                   colours.level,
                   colours.histogram_entropy,
                ]
        
        hue_fns = [
                ]
        
        sat_fns = [
                ]
        val_fns = [
                   textures.entropy
                ]
        
        features = []
        for c in chroma_fns:
            features.append([c(chroma)])
        for h in hue_fns:
            features.append([h(hue)])
        for s in sat_fns:
            features.append([s(sat)])
        for v in val_fns:
            features.append([v(val)])
        
        return np.array(features)

class UnserColourContextFeatures(ImageFeatures):
    
    D = 4
    
    def process_image(self, img):
        hsv = rgb2hsv(img)
        hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
        
        chroma = sat*val
        
        assert np.any(chroma <= 1), 'Combined saturation and value should be less than 1'
        
        h_s = sum_histogram(chroma, self.D, self.D) #TODO: should this be 2?
        h_d = difference_histogram(chroma, self.D, self.D)
        
        features = []
        m = mean(h_s)
        features.append(m)                              # 0
        features.append(variance(h_s, h_d, m))          # 1
        features.append(energy(h_s, h_d))               # 2
        features.append(correlation(h_s, h_d, m))       # 3
        features.append(entropy(h_s, h_d))              # 4
        features.append(contrast(h_d))                  # 5
        features.append(homogeneity(h_d))               # 6
        features.append(cluster_shade(h_s, m))          # 7
        features.append(cluster_prominence(h_s, m))     # 8
        
        
        # Colour Features
        chroma_fns = [
                   colours.contrast,                    # 9
#                   colours.level,                       #TODO: remove 10
                   colours.histogram_entropy,           # 10
                ]
        
        hue_fns = [
                ]
        
        sat_fns = [
                   colours.level,                       # 11
                ]
        val_fns = [
#                   textures.entropy                     # 12
                ]
        
        for c in chroma_fns:
            features.append(c(chroma))
        for h in hue_fns:
            features.append(h(hue))
        for s in sat_fns:
            features.append(s(sat))
        for v in val_fns:
            features.append(v(val))
            
        return np.array(features)[:,np.newaxis]
