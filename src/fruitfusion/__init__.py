import sys
if 'pydevd' in sys.modules:
    DEBUG = True
    N_JOBS = 1
else:
    DEBUG = False
    N_JOBS = -2