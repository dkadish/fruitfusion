import sys, os
sys.path.append(os.path.abspath(os.path.join(__file__,'../../')))

from skimage.filter import rank
from skimage.color import rgb2hsv
from skimage.filter import canny, sobel, prewitt, median_filter
from skimage.filter.lpi_filter import forward
from fruitfusion.tools import plot_img_and_hist, plot_img

from skimage.filter import LPIFilter2D
from skimage.exposure import exposure
from fruitfusion.background import regions
from skimage.io import imread
from skimage.feature import local_binary_pattern
from skimage.morphology import disk
import numpy as np

'''
SMC Papers on focal images.
'''

N_SEGMENTS = 50
RATIO = 10

from skimage.segmentation import slic
def noise_level_estimation(img, n_segments=N_SEGMENTS, ratio=RATIO):
    '''
    
    NO: !Based on "Noise Estimation from a Single Image", Liu et. al, 2006
    "Adaptive Temporal Filtering for CFA Video Sequences" Bosco et.al., 2002
    "Fast Method for Noise Level Estimation and "Integrated Noise Reduction", Bosco, 200?
    '''
    # Segment the image according to k-means
    segments = slic(img, n_segments, ratio)
    
    # Creating the structures for the comparison
    uni = np.unique(segments)
    comp = np.ones((uni.size,) + img.shape) * uni[:,np.newaxis,np.newaxis]
    
    masks = segments == comp
    
    level = []
    for mask in masks:
        maskimg = img[mask]
        
        dev = np.std(maskimg)
        
        level.append(dev * maskimg.size)
    
    return np.sum(level) / img.size

def lbp(img, mask=None, visualize=False, output=None):
    METHOD = 'uniform'
    P = 16
    R = 2
    
    if mask == None:
        lbp_img = local_binary_pattern(img, P, R, METHOD)
        hist, bin_edges = np.histogram(lbp_img, normed=True, bins=P + 2, range=(0, P + 2))
        
    else:
        lbp_img = local_binary_pattern(img, P, R, METHOD)
        lbp_img[mask == 0] = -1
        hist, bin_edges = np.histogram(lbp_img, normed=True, bins=P + 2, range=(0, P + 2))

    if visualize or output!=None: plot_img_and_hist(lbp_img, bin_edges[:-1], hist, xlabel='Histogram Bin', ylabel='Percentage in Bin', title=None, colour='hot', visualize=visualize, output=output)
        
    return hist[:,np.newaxis], bin_edges

def edge_strength(img, mask=None, visualize=False):
    edges = prewitt(img, mask=mask)
    
    if visualize:
        plot_img(edges)
        print 'Edge Strength: ', np.sum(edges)
    
    return np.average(edges)

def fft(img, mask=None, visualize=False):
    '''The average length of the fourier transform value.
    '''
    ft = np.fft.fft2(img)
    a = np.abs(ft)
    return np.average(a)
    
def gaussian_filter(r, c):
    return np.exp(-np.hypot(r, c)/1)

def filter_difference(filter, img, mask=None, visualize=False):
    
    blurred = filter(img, mask=mask)
    diff = np.abs(img - blurred)

    if mask == None:
        lpd = np.average(diff)
    else:
        lpd = np.average(diff[mask!=0])
    
    if visualize:
        print 'Difference: ', lpd
        plot_img(diff)
        
    return lpd

def low_pass_difference(img, mask=None, visualize=False):
    return filter_difference(lambda f, mask: forward(f, gaussian_filter), img, mask, visualize)

def median_difference(img, mask=None, visualize=False):
    return filter_difference(median_filter, img, mask, visualize)

def entropy(img, mask=None, visualize=False):
    grey = np.uint8(255*img)
    ent = rank.entropy(grey, disk(5))
    if visualize: print 'Entropy: ', np.average(ent), np.var(ent)
    return np.average(ent)
    
def _main_edges(args):
    img = imread(args.img)
    hsv = rgb2hsv(img)
    hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
    
    segmentation, labeled_value = regions(sat)
    
    edge_strength(val, segmentation, visualize=True)
    
def _main_lbp(args):
    img = imread(args.img)
    hsv = rgb2hsv(img)
    hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
    
    segmentation, labeled_value = regions(sat)
    
    lbp(val, segmentation, visualize=True, output=args.output)
    
def _main_lpd(args):
    img = imread(args.img)
    hsv = rgb2hsv(img)
    hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
    
    segmentation, labeled_value = regions(sat)
    
    low_pass_difference(val, segmentation, visualize=True)
    
def _main_median(args):
    img = imread(args.img)
    hsv = rgb2hsv(img)
    hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
    
    segmentation, labeled_value = regions(sat)
    
    median_difference(val, visualize=True)
    
def _main_fft(args):
    img = imread(args.img)
    hsv = rgb2hsv(img)
    hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
    
    segmentation = None
    
    fft(val, segmentation, visualize=True)
    
def _main_entropy(args):
    img = imread(args.img)
    hsv = rgb2hsv(img)
    hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
    
    segmentation = None
    
    entropy(val, segmentation, visualize=True)

if __name__=='__main__':
    '''Example:
    python textures.py -i ~/documents/school/grad/projects/ai/tropical-fruits/agata_potato/agata_potato_016.jpg hist
    '''
    
    import argparse
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-i', dest='img', help='Input image file')
    parser.add_argument('-o', dest='output', help='Where to output the image file')
    
    subparsers = parser.add_subparsers(help='commands')
    lbp_parser = subparsers.add_parser('lbp', help='Extracts local binary pattern histograms from images.')
    lbp_parser.set_defaults(func=_main_lbp)
    
    edges_parser = subparsers.add_parser('edges', help='Extracts edge strengths from images.')
    edges_parser.set_defaults(func=_main_edges)
    
    edges_parser = subparsers.add_parser('lpd', help='Extracts low pass difference from images.')
    edges_parser.set_defaults(func=_main_lpd)
    
    edges_parser = subparsers.add_parser('median', help='Extracts median difference from images.')
    edges_parser.set_defaults(func=_main_median)
    
    edges_parser = subparsers.add_parser('fft', help='Extracts fft from images.')
    edges_parser.set_defaults(func=_main_fft)
    
    edges_parser = subparsers.add_parser('entropy', help='Extracts entropy from images.')
    edges_parser.set_defaults(func=_main_entropy)
    
    args = parser.parse_args()
    args.func(args)