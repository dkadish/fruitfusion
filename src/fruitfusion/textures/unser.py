'''Functions relating to Unser's Descriptors
These are described in:
Unser (1986) Sum and Difference Histograms for Texture Classification
Rocha (2010)

Created on 2013-03-11

@author: dkadish
'''

import numpy as np

'''Local descriptors
'''
def summate(img, di, dk):
    return img[:-di,:-dk] + img[di:,dk:]

def difference(img, di, dk):
    return img[:-di,:-dk] - img[di:,dk:]

def sum_histogram(img, di, dk):
    return _h(img, di, dk, op='sum')

def difference_histogram(img, di, dk):
    return _h(img, di, dk, op='diff')

def _h(img, di, dk, op):
    h = op == 'sum' and summate or op == 'diff' and difference or None
    range = (0.0, op=='sum' and 2.0 or op=='diff' and 1.0)
    assert h != None, 'Operation must be either sum or diff, (%s)' %str(op)
    
    # Make sure the image is in the right format.
    
    histogram = np.histogram(h(img, di, dk).flatten(), bins=32, range=range)[0]
    
    # Normalize the histogram
    histogram = np.float_(histogram) / np.sum(histogram)
    
    return histogram

'''Global Descriptors.
To be used as part of the context
'''
def mean(h_s):
    '''The mean pixel value in the image.
    Unser (1986), Table II, Equation 1
    '''
    return (0.5) * np.sum(np.arange(h_s.size) * h_s)

def variance(h_s, h_d, m=None):
    '''
    Unser (1986), Table II, Equation 2
    '''
    u = m != None and m or mean(h_s)
    i = np.arange(h_s.size)
    j = np.arange(h_d.size)
    return 0.5 * (np.sum((i-2*u)**2 * h_s) + np.sum(j**2 * h_d))

def energy(h_s, h_d):
    '''
    Unser (1986), Table II, Equation 3
    '''
    return np.sum(h_s**2) * np.sum(h_d**2)

def correlation(h_s, h_d, m=None):
    '''
    Unser (1986), Table II, Equation 4
    '''
    u = m != None and m or mean(h_s)
    i = np.arange(h_s.size)
    j = np.arange(h_d.size)
    return 0.5 * (np.sum((i-2*u)**2 * h_s) - np.sum(j**2 * h_d))

def entropy(h_s, h_d):
    '''
    Unser (1986), Table II, Equation 5
    '''
    hs = h_s[h_s != 0]
    hd = h_d[h_d != 0]
    return - np.sum(hs * np.log(hs)) - np.sum(hd * np.log(hd))

def contrast(h_d):
    '''
    Unser (1986), Table II, Equation 6
    '''
    j = np.arange(h_d.size)
    return np.sum(j**2 * h_d)

def homogeneity(h_d):
    '''
    Unser (1986), Table II, Equation 7
    
    This is wrong in Rocha (2010)
    '''
    j = np.arange(h_d.size)
    return np.sum((1/(1 + j**2)) * h_d)

def cluster_shade(h_s, m = None):
    '''
    Unser (1986), Table II, Equation 8
    '''
    u = m != None and m or mean(h_s)
    i = np.arange(h_s.size)
    return np.sum((i - 2*u)**3 * h_s)

def cluster_prominence(h_s, m = None):
    '''
    Unser (1986), Table II, Equation 9
    '''
    u = m != None and m or mean(h_s)
    i = np.arange(h_s.size)
    return np.sum((i - 2*u)**4 * h_s)