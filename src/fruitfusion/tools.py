import matplotlib
from matplotlib.figure import SubplotParams
matplotlib.use('GTK')
import matplotlib.pyplot as plt

SIZE = 'x-large'

def plot_img_and_hist(img, bins, hist, colour=None, title=None, xlabel=None, ylabel=None, visualize=True, output=None):
    img_fig = plt.figure()
#     plt.subplot(211)
    if colour != None:
        plt.imshow(img, cmap=colour)
    else:
        plt.imshow(img)
        
    plt.axis('off')
    
    hist_fig = plt.figure(figsize=(11,3.25), subplotpars=SubplotParams(bottom=0.25))
    hist_fig.SubplotParams(bottom=0.15)
#     plt.axes().set_aspect(aspect=1.0)
# #     plt.subplot(212)
#     plt.plot(bins, hist)
    if ylabel: plt.ylabel(ylabel, size=SIZE)
    if xlabel: plt.xlabel(xlabel, size=SIZE)
#     if title != None: plt.title(title, size=SIZE)
#     #plt.axis('off')
    
    if output != None:
        from matplotlib.backends.backend_pdf import PdfPages
        pp = PdfPages(output)
        img_fig.savefig(pp, format='pdf')
        hist_fig.savefig(pp, format='pdf')
        pp.close()
        
    if visualize: plt.show()
    
def plot_img(img):
    plt.imshow(img)
    plt.axis('off')
    plt.show()
    
def cachedproperty(f):
    '''cachedproperty defines a decorator for a read-only property that sets
    its value using the decorated function on the first call and then returns
    that value in later calls
    '''
    name = '_' + f.__name__
    def getter(self):
        if not hasattr(self, name) or getattr(self,name) == None:
            setattr(self, name, f(self))
        return getattr(self, name)
    return property(getter)