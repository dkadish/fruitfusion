'''
Created on May 13, 2013

@author: dkadish
'''
import numpy as np
from sklearn.neighbors import BallTree

from fruitfusion.context.multiclass.base import LocatorBase
from scipy.stats.kde import gaussian_kde
from itertools import izip
from fruitfusion.context.multiclass import dict_to_array

def correctness(X, y, classifiers):
    '''Finds out when each classifiers was correct.
    
    Return
    ======
    l : dict { primary: np.array [n_samples, ] }
    '''
    l = {}
    for p in classifiers:
        loc = np.zeros(y.shape)
        prediction = classifiers[p].predict(X[p])
        loc[ prediction == y ] = 1
        l[p] = loc
    return l

def unique_rows(a):
    '''Finds unique rows in a numpy array.
    
    From: http://stackoverflow.com/a/8567929
    '''
    unique_a = np.unique(a.view([('', a.dtype)]*a.shape[1]))
    return unique_a.view(a.dtype).reshape((unique_a.shape[0], a.shape[1]))

def unique_position(a):
    pos = np.zeros((a.shape[0],))
    
    rows = unique_rows(a)
    for i in xrange(rows.shape[0]):
        pos[a == rows[i,:]] = i
        
    return pos

class SampleCorrectnessLocator(LocatorBase):
    '''Determines the locale of a sample based on whether the primary classifier
    was correct or incorrect in its prediction.
    
    The locale is either 1 or 0.
    '''
    
    def locate(self, X, y, X_ctx):
        return correctness(X, y, self.cdf.primary)

class NeighbourCorrectnessLocatorBase(LocatorBase):
    N_LOCALES = 3
    __DEFAULT_OUTTYPE = 'level'
    
    def __init__(self, cdf, n=None, outtype=None):
        super(NeighbourCorrectnessLocatorBase, self).__init__(cdf)
        self.n = n == None and self.N_LOCALES or n
        
        self.outtype = outtype == None and self.__DEFAULT_OUTTYPE or outtype

    def locate(self, X, y, X_ctx):
        correct = correctness(X, y, self.cdf.primary)
        
        # Find the indices of the nearest K points in the context
        k_dists, k_inds = self._get_local_points(X_ctx)
        
        lc = {}
        for p in self.cdf.primary:
            # Find the percentage of the nearest points that were correct
            local_scores = self._get_local_scores(correct[p], k_inds, k_dists)
            
            assert local_scores.shape[0] == X_ctx.shape[0], 'Should still have the same number of samples'
            
            if self.outtype == 'level':
                lc[p] = self._get_local_class(local_scores)
            elif self.outtype == 'score':
                lc[p] = local_scores
            
        return lc
    
    def _get_local_points(self, X_ctx):
        pass
    
    def _get_local_scores(self, y_correct, k_inds, k_dists):
        '''
        Parameters
        ==========
        y_correct : 
            The samples that were classified correctly
        '''
        k_near_correct = y_correct[k_inds.ravel()].reshape(k_inds.shape)
        local_score = np.average(k_near_correct,axis=1)
        return local_score
    
    def _get_local_class(self, local_scores):
        
        local_class = np.zeros(local_scores.shape)            
        for j,s in enumerate(np.arange(0.0,1.0,1.0/self.n)):
            local_class[np.logical_and(local_scores>=s,local_scores<=(s+1.0/self.n))] = j
        
        return local_class

class KNeighbourCorrectnessLocator(NeighbourCorrectnessLocatorBase):
    K_NEIGHBOURS = 5
    
    def __init__(self, cdf, n=None, k=None):
        super(KNeighbourCorrectnessLocator, self).__init__(cdf, n)
        self.k = k == None and self.K_NEIGHBOURS or k
        
    def _get_local_points(self, X_ctx):
        ball_tree = BallTree(X_ctx)
        return ball_tree.query(X_ctx, k=self.k)

class RadiusNeighbourCorrectnessLocator(NeighbourCorrectnessLocatorBase):
    R_NEIGHBOURS = 0.025
    
    def __init__(self, cdf, n=None, r=None, outtype=None):
        super(RadiusNeighbourCorrectnessLocator, self).__init__(cdf, n, outtype)
        self._r = r == None and self.R_NEIGHBOURS or r
        self.r = None
    
    def _get_local_points(self, X_ctx):
        ball_tree = BallTree(X_ctx)
        if self.r == None:
            self.r = np.sqrt(X_ctx.shape[1] * np.power(self._r, 2))
        
        ind, dist = ball_tree.query_radius(X_ctx, self.r, return_distance=True)
        return dist, ind
    
    def _get_local_scores(self, y_correct, k_inds, k_dists):
        local_scores = np.zeros(len(k_inds))
        for i, inds in enumerate(k_inds):
            local_scores[i] = np.average(y_correct[inds])
        
        return local_scores
    
class WeightedRadiusNeighboursCorrectnessLocator(RadiusNeighbourCorrectnessLocator):
    R_NEIGHBOURS = 0.01
    
    def _get_local_scores(self, y_correct, k_inds, k_dists):
        local_scores = np.zeros(len(k_inds))
        for i, inds in enumerate(k_inds):
            weighted_score = y_correct[inds] * (1.0/np.exp(k_dists[i]/self.r/2.0))
            local_scores[i] = np.average(weighted_score)
        
        return local_scores
    
class WeightedRadiusNeighboursZoneLocator(WeightedRadiusNeighboursCorrectnessLocator):
    __DEFAULT_THRESH = [0.8, 0.9]
    
    def __init__(self, cdf, r=None, thresh=None):
        WeightedRadiusNeighboursCorrectnessLocator.__init__(self, cdf, r=r, outtype='score')
        self.thresh = thresh == None and self.__DEFAULT_THRESH or thresh
    
    def locate(self, X, y, X_ctx):
        lc = dict_to_array(WeightedRadiusNeighboursCorrectnessLocator.locate(self, X, y, X_ctx))
        
        locales, self.locale_sets = sets_from_scores(lc, self.thresh)
        
        return locales
    
class AugmentedCorrectnessLocator(LocatorBase):
    
    def locate(self, X, y):
        correct = correctness(X, y, self.cdf.primary)
        
        lc = {}
        for p in self.cdf.primary:
            # Create an X variable that incorporates the correctness.
            # This should emphasize that new axis
            
            # Fit_transform a clustering algorithm to the new X variable
            
            # Use the transformed value as the locale in that primary
            pass
        
        return lc
    
class KNeighbourLocator(LocatorBase):
    
    def locate(self, X, y, X_ctx):
#         # Find the indices of the nearest K points in the context
#         self.ball_tree = BallTree(X_ctx)
#         k_dists, k_inds = self.ball_tree.query(X_ctx, k=self.k)
#         
#         k_inds = np.sort(k_inds, axis=1) # Get the closest points sorted
#         
#         locales = unique_position(k_inds)
#         return dict([(p, locales) for p in self.cdf.primary])
        return None
    
class GaussianKDELocator(LocatorBase):
    '''Generate a gaussian kde of the correct and incorrect points.
    Label areas by the different classifiers/classifier combinations that perform best.
    '''
    __DEFAULT_THRESH = [0.8, 0.9]
    
    def __init__(self, cdf, factor=None, thresh=None):
        LocatorBase.__init__(self, cdf)
        self.factor = factor
        self.thresh = thresh == None and self.__DEFAULT_THRESH or thresh
        self.locale_sets = []
        
#         self._d_min, self._d_max = {}, {}
        self._d_avg, self._d_std = {}, {}
    
    def locate(self, X, y, X_ctx):
        '''
        '''
        correct = correctness(X, y, self.cdf.primary)
        
        # Create the density estimations
        self._init_density_estimators(X_ctx, correct)
        
        # Sort the primaries by performance
        densities = self.densities(X_ctx)
#         rankings = np.argsort(densities, axis=1)
#         
#         # Initially set the locale as the max performer
#         locale = rankings[:,-1].copy()
#         score_at_rank = lambda i: densities[np.arange(densities.shape[0]),rankings[:,i]]
#         
#         # Add other rankings when the top N classifiers are close
#         for i in xrange(2,rankings.shape[1]+1):
#             score_ratio = score_at_rank(-i)/score_at_rank(-1)
#             for low,high in izip(self.thresh,self.thresh[1:]+[1.0]):
#                 zeroed_sr = np.abs(score_ratio-1)
#                 samples_inds = np.logical_and( zeroed_sr < 1 - low, zeroed_sr > 1-high ) # This is necessary to deal with negative scores
#                 clf_sets = np.sort(rankings[samples_inds,-i:], axis=1)
#                 
#                 # Code from http://numpy-discussion.10968.n7.nabble.com/unique-rows-of-array-td23290.html for unique rows
#                 unique_sets = np.unique(clf_sets.view([('',clf_sets.dtype)]*clf_sets.shape[1])).view(clf_sets.dtype).reshape(-1,clf_sets.shape[1])
#                 
#                 for s in unique_sets:
#                     si = samples_inds.copy()
#                     si_in_set = np.all(clf_sets == s,axis=1)
#                     si[si] = si_in_set
#                     locale[si] = np.max(locale)+1
#                     
#                     self.locale_sets.append(s)
        locale, self.locale_sets = sets_from_scores(densities, self.thresh)
                
        return locale

    def density(self, p, X):
        d = self.positive_density[p](X.T) - self.negative_density[p](X.T)
        
#         if not p in self._d_min and not p in self._d_max:
#             self._d_min[p], self._d_max[p] = np.min(d), np.max(d)
        if not p in self._d_avg and not p in self._d_std:
            self._d_avg[p], self._d_std[p] = np.average(d), np.std(d)
        
#         d = (d - self._d_min[p])/(self._d_max[p] - self._d_min[p])
        d = (d - self._d_avg[p])/self._d_std[p]
        
        return d
    
    def densities(self, X):
        return np.array([self.density(p, X) for p in self.cdf.primary]).T
    
    def _init_density_estimators(self, X, y):
        '''
        X : np.array []
            the context space
        y : dict {p: np.array []}
            the correctness of classifier p
        '''
        self.positive_density = {}
        self.negative_density = {}
        
        for p in self.cdf.primary:
            if self.factor != None:
                self.positive_density[p] = gaussian_kde(X[y[p]==1].T, bw_method=self.factor)
                self.negative_density[p] = gaussian_kde(X[y[p]==0].T, bw_method=self.factor)
            else:
                self.positive_density[p] = gaussian_kde(X[y[p]==1].T, bw_method='silverman')
                self.negative_density[p] = gaussian_kde(X[y[p]==0].T, bw_method='silverman')
                
def sets_from_scores(scores, thresh):
    rankings = np.argsort(scores, axis=1)
    locale_sets = []
    
    # Initially set the locale as the max performer
    locale = rankings[:,-1].copy()
    score_at_rank = lambda i: scores[np.arange(scores.shape[0]),rankings[:,i]]
    
    # Add other rankings when the top N classifiers are close
    for i in xrange(2,rankings.shape[1]+1):
        score_ratio = score_at_rank(-i)/score_at_rank(-1)
        for low,high in izip(thresh,thresh[1:]+[1.0]):
            zeroed_sr = np.abs(score_ratio-1)
            samples_inds = np.logical_and( zeroed_sr < 1 - low, zeroed_sr >= 1-high ) # This is necessary to deal with negative scores
            clf_sets = np.sort(rankings[samples_inds,-i:], axis=1)
            
            # Code from http://numpy-discussion.10968.n7.nabble.com/unique-rows-of-array-td23290.html for unique rows
            unique_sets = np.unique(clf_sets.view([('',clf_sets.dtype)]*clf_sets.shape[1])).view(clf_sets.dtype).reshape(-1,clf_sets.shape[1])
            
            for s in unique_sets:
                si = samples_inds.copy()
                si_in_set = np.all(clf_sets == s,axis=1)
                si[si] = si_in_set
                locale[si] = np.max(locale)+1
                
                locale_sets.append(s)
                
    return locale, locale_sets