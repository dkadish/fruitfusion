'''
Created on May 14, 2013

@author: dkadish
'''
import numpy as np

from fruitfusion.context.multiclass.base import FuserBase
from fruitfusion.context.multiclass.locator import correctness
from fruitfusion.context.multiclass import dict_to_array

class WeightedVoting(FuserBase):
    ''' Assumes that locales are in the range of 0..L-1, where L is the number of the locale? CHECK THIS!
    '''
    
    def fit(self, X, y, locales):
        '''
        Parameters
        ==========
        X : dict { primary | 'context': np.array [n_samples, n_features] }
            The dictionary of features to fit to.
        y : dict { primary | 'context': np.array [n_samples,] }
            The dictionary of the correct class | locale for each sample.
            
        Fits the voting mechanism by setting the weights matrix.
        Weights is a [n_classifiers x n_locales] np.array where weights[i,j] is 
        the weight of the ith classifier in the jth locale.
        '''
        self.correctness = correctness(X, y, self.cdf.primary)
        self.n_classes = np.unique(y).size
        
        # Find the average performance in each locale for each classifier
        #TODO: Make this matrix-y
        self.weights = np.zeros(shape=(len(self.cdf.primary), len(self.cdf.segmenter.locales)))
        for i in self.correctness:
            for j, locale in enumerate(self.cdf.segmenter.locales):
                if np.any(locales == locale): #FIXME: Why are there some that don't exist? Is that because of the sectioning method?
                    self.weights[i,j] = np.average(self.correctness[i][locales == locale])
                
    def predict(self, X, locale):
        '''
        Parameters
        ==========
        X : np.array [n_samples, n_classifiers]
            The array of predictions from the primary classifiers to fuse
        locale : np.array [n_samples, 1]
            An array containing the locale for each of the samples in X
        '''
        X = dict_to_array(X) # Gets X as a numpy array in the format [n_features, n_classifiers]
        
        n_samples = X.shape[0]
        n_classifiers = X.shape[1]#len(self.cdf.primary)
#         n_classes = self.n_classes
#         
#         '''Create a [n_samples, n_classifiers, n_classes] np.array and iterate
#         over the classes, setting each cell along the classes axis as 1 if that
#         sample/classifier combination picked it as the most likely class.
#         '''
#         votes = np.zeros((n_samples, n_classifiers, n_classes))
#         for c in range(self.n_classes):
#             votes[X == c, c] = 1
        
        # [n_samples, n_classifiers] np.array where M[i,j] is the locale weight for classifier j for the ith sample
        sample_weights = np.zeros((n_samples, n_classifiers))
        for p in range(n_classifiers):
            sample_weights[:,p] = self.weights[p,self.locale_index(locale)]
        
#         # [n_samples, n_classes] matrix where M[i,j] is the weight of votes for class j for the ith sample
#         weighted_votes = votes * sample_weights[:,:,np.newaxis]
#         weighted_vote = np.sum(weighted_votes, axis=1)
#         
#         return np.argmax(weighted_vote, axis=1)
    
        return weighted_vote_result(X, sample_weights, classes=None)#np.arange(self.n_classes))
    
    def locale_index(self, locale):
        if type(locale) == np.ndarray:
            li = np.zeros(locale.shape, np.int)
            for l in np.unique(locale):
                li[locale == l] = np.argwhere(self.cdf.segmenter.locales == l)
            return li
        else:
            return np.argwhere(self.cdf.segmenter.locales == locale)[0,0]

# class NeighbourRankSelection(FuserBase):
#     
#     def fit(self, X, y, locales):
#         self.primary = 

class ClassifierRankSelection(FuserBase):
    
    def fit(self, X, y, locales):
        '''Figure out which classifiers are best in each locale.
        Store that in an array'''
#         self.correctness = correctness(X, y, self.cdf.primary)
#         correctness_ = dict_to_array(self.correctness)
#         
#         locale_best = []
#         for locale in self.cdf.segmenter.locales:
#             lb = np.average(correctness_[locales == locale], axis=0)
#             
#             assert lb.size == len(self.cdf.primary), 'The lb is the wrong size.'
#             
#             locale_best.append(np.argmax(lb))
#         
#         self.locale_best = np.array(locale_best)
        pass
    
    def predict(self, X, locale):
#         p = dict_to_array(X)
#         
#         locale_tf = np.repeat(self.cdf.segmenter.locales[:,np.newaxis], repeats=locale.shape[0], axis=1) == locale
#         self._selected = np.repeat(self.locale_best[:,np.newaxis], repeats=locale.shape[0], axis=1)[locale_tf]
#         
#         self._selected = self.locale_best[self.cdf.segmenter.locales == locale]
#         
#         return p[np.arange(p.shape[0]),self._selected]
        x = dict_to_array(X)
        self._selected = locale
        return x[np.arange(0,x.shape[0]), locale]
    
    @property
    def predict_selected(self):
        return self._selected
    
class NeighboursClassifierBase(FuserBase):
    
    def _performance_matrix(self, neighbours):
        '''
        Return
        ======
        mat : np.array [n_samples, k_neighbours, n_classifiers]
        '''
        mat = np.zeros((neighbours.shape[0], neighbours.shape[1], len(self.cdf.primary)))
        
        # For each sample
        #FIXME: Can I do this without the loop? 
        for s in xrange(neighbours.shape[0]):
            mat[s,:,:] = self.correctness[neighbours[s,:]] # np.array [k_neighbours, n_classifiers]
        
        return mat
    
    def local_performance(self, neighbours):
        performance = np.average(self._performance_matrix(neighbours), axis=1) # np.array [n_samples, n_classifiers]
        return performance
    

class NeighboursClassifierRankSelection(NeighboursClassifierBase):
    '''Implements Classifier Rank Algorithm from Woods1997 (section 2.3).
    '''
    
    def fit(self, X, y, locales):
        self.correctness = dict_to_array(correctness(X, y, self.cdf.primary)) # np.array [n_samples, n_classifier] ??
        
    def predict(self, X, locale):
        '''The CRS Fuser has to select the best-performing classifier in the
        locale. This locale is defined by which points are the k-nearest
        neighbours.
        
        The predictor needs access to the performance of the primary
        classifiers in that locale. OR it needs to know which points were
        correctly classified by the primary classifiers in training.
        '''
        X = dict_to_array(X)
        
        # Decode the locales
        neighbours = self._decode_locales(locale)
        
        # Find the performance of each primary classifier in the locale
        performance_matrix = self._performance_matrix(neighbours)
        
        # Average the classifier performance for each point in the locale
        performance = np.average(performance_matrix, axis=1) # np.array [n_samples, n_classifiers]
        
        self._selected = np.argmax(performance, axis=1)
        
        result = X[np.arange(self._selected.size), self._selected]
         
        # Tiebreaker, WTF?
        best_score = np.max(performance, axis=1)[:,np.newaxis] # Best Scores
        n_highest = np.sum(performance == best_score, axis=1) # How many are tied?
        
        self.n_highest = n_highest
         
        X_tied = X[n_highest > 1, :] # Votes
        result_tied = result[n_highest > 1] # Predictions
        max_tied = best_score[n_highest > 1] # Highest Scores
        performance_tied = performance[n_highest > 1] # Highest Performance scores
         
        for i in xrange(X_tied.shape[0]):
            vote_count = np.bincount(X_tied[i,max_tied[i]==performance_tied[i,:]])
            class_max = np.argmax(vote_count)
             
            # Check for another tie
            if np.sum(vote_count == vote_count[class_max]) > 1:
                result_tied[i] = np.random.permutation(np.arange(0,vote_count.size)[vote_count==vote_count[class_max]])[0]
            else:
                result_tied[i] = class_max
            self._selected[n_highest > 1][i] = np.argmax(performance_tied[i,:] * (X_tied[i,:] == class_max))
               
        result[n_highest > 1] = result_tied
          
        
        # Return the result of the best classifier
        return result # np.array [n_samples] #TODO: right axis?
    
    def _decode_locales(self, locale):
        '''
        Return
        ======
        neighbours : np.array [n_samples, k_neighbours]
        '''
        return locale
    
    @property
    def predict_selected(self):
        return self._selected
    
def ModifiedClassifierRankSelection(ClassifierRankSelection):
    '''Implements Modified Classifier Rank Algorithm from Woods1997 (section 2.4).
    
    Honestly, I probably won't implement this. It isn't necessary for my writing.
    '''
    
    def predict(self, X, locale):
        '''
        '''
        pass
    
class SelectOrVoteFuser(FuserBase):
    '''Simple non-optimized weighted vote, or single classifier.
    '''
    __DEFAULT_THRESH = 0.8
    
    def __init__(self, cdf, thresh=None):
        FuserBase.__init__(self, cdf)
        
        self.thresh = thresh == None and self.__DEFAULT_THRESH or thresh
    
    def fit(self, X, y, locales):
        '''
        X : dict {p: np.array }
            Features
        y : dict {p: np.array }
            Truth
        locales : np.array [n_samples, n_neighbours]
            Array of nearest points
        '''
        
        
        
        self.correctness = dict_to_array(correctness(X, y, self.cdf.primary)) # np.array [n_samples, n_classifiers]
        predictions = dict_to_array(self.cdf.primary.predict(X)) # np.array [n_samples, n_classifiers]
        
        # Find the best selected and voting classifiers for each locale
        max_locale = np.max(self.cdf.segmenter.locales)
        self.locale_score = np.zeros(shape=(max_locale+1))
        self.method = np.zeros(shape=(max_locale+1))
        self.individual = np.zeros(shape=(max_locale+1), dtype=np.int8) # np.array [n_locales], the index of the best individual classifier
        self.weighted_voting = np.zeros(shape=(max_locale+1, len(self.cdf.primary))) # np.array [n_locales, n_classifiers], weights for the n_classifiers
        self.ranked_voting = np.zeros(shape=(max_locale+1, len(self.cdf.primary))) # np.array [n_locales, n_classifiers], weights for the n_classifiers
        
        # Find the best individual classifier
        for l in self.cdf.segmenter.locales:
            #TODO: redo this with repeats
            if (locales == l).any():
                self.weighted_voting[l,:] = np.average(self.correctness[locales == l, :], axis=0)
            self.ranked_voting[l,:] = np.float64(self.weighted_voting[l,:].size - np.argsort(self.weighted_voting[l,:]))
            self.individual[l] = np.argmax(self.weighted_voting[l,:])
        
            ind_score = np.max(self.weighted_voting[l,:]) # Individual score
            
            # Ranked voting score
            below_thresh = self.weighted_voting[l,:]/self.weighted_voting[l,-1] < self.thresh
            above_thresh = np.logical_not(below_thresh)
            self.ranked_voting[l,below_thresh] = 0
            a = self.ranked_voting[l,above_thresh]
            self.ranked_voting[l,above_thresh] = (a - np.min(a))/((np.max(a) - np.min(a))*2) + 0.5
            rv_predict = weighted_vote_result(predictions[locales == l,:], np.repeat(self.ranked_voting[l,np.newaxis], repeats=predictions[locales == l,:].shape[0], axis=0), classes=np.unique(predictions))
            rvote_score = np.average(rv_predict == y[locales == l])
            
            wv_predict = weighted_vote_result(predictions[locales == l,:], np.repeat(self.weighted_voting[l,np.newaxis], repeats=predictions[locales == l,:].shape[0], axis=0), classes=np.unique(predictions))
            wvote_score = np.average(wv_predict == y[locales == l])
            
            if ind_score > rvote_score and ind_score > wvote_score:
                self.method[l] = 0
                self.locale_score[l] = ind_score
            elif rvote_score >= wvote_score:
                self.method[l] = 1
                self.locale_score[l] = rvote_score
            else:
                self.method[l] = 2
                self.locale_score[l] = wvote_score
                
        self.cdf._locale_scores = self.locale_score
        
        print 'Fusion Method: ', self.method
                    
    def predict(self, X, locale):
        predictions = dict_to_array(X)
        m_select = self.method[locale] == 0
        m_rvote = self.method[locale] == 1
        m_wvote = self.method[locale] == 2
        
        prediction = np.zeros(shape=(locale.size))
        
        # Handle selection zones
        if np.any(m_select):
            s_pred = predictions[m_select, self.individual[locale[m_select]]]
            prediction[m_select] = s_pred
        
        # Handle voting zones
        if np.any(m_rvote):
            v_pred = weighted_vote_result(predictions[m_rvote,:], self.ranked_voting[locale[m_rvote],:], classes=np.unique(predictions))
            prediction[m_rvote] = v_pred
        
        # Handle voting zones
        if np.any(m_wvote):
            v_pred = weighted_vote_result(predictions[m_wvote,:], self.weighted_voting[locale[m_wvote],:], classes=np.unique(predictions))
            prediction[m_wvote] = v_pred
            
        print 'Individual Distribution: ', np.unique(self.individual[locale[m_select]]), np.bincount(self.individual[locale[m_select]])
        print 'Fusion Type Count: ', np.sum(m_select), np.sum(m_rvote), np.sum(m_wvote)
        
        return prediction
    
class NeighbourSelectOrVoteFuser(NeighboursClassifierBase):
    '''Simple non-optimized weighted vote, or single classifier.
    '''
    __DEFAULT_THRESH = 0.8
    
    def __init__(self, cdf, thresh=None):
        FuserBase.__init__(self, cdf)
        
        self.thresh = thresh == None and self.__DEFAULT_THRESH or thresh
    
    def fit(self, X, y, locales):
        self.correctness = dict_to_array(correctness(X, y, self.cdf.primary)) # np.array [n_samples, n_classifiers]
        self.predictions = dict_to_array(self.cdf.primary.predict(X)) # np.array [n_samples, n_classifiers]
        self.truth = y
        
    def predict(self, X, locale):
        X = dict_to_array(X)
        performance = self.local_performance(locale) # np.array [n_samples, n_classifiers]
        
#         weighted_votes = performance # The weighting is equal to the performance in the kNN
        individual = np.argmax(performance, axis=1)
        individual_performance = np.max(performance, axis=1)
        
        # Find the scores for weighted voting
        weighted_result = np.zeros(locale.shape) # np.array [n_samples, n_locales]
        for s in xrange(locale.shape[0]):
            weighted_result[s,:] = weighted_vote_result(predictions=self.predictions[locale[s,:]], weights=np.repeat(performance[s,np.newaxis], repeats=self.predictions[locale[s,:]].shape[0], axis=0), classes=np.unique(X))
        
        weighted_performance = np.average(self.truth[locale] == weighted_result,axis=1)
        
        # Find the scores for ranked voting
        ranked_result = np.zeros(locale.shape) # np.array [n_samples, n_locales]
        ranked_weighting = performance.copy()
        ranked_weighting = ranked_weighting/ranked_weighting[np.arange(ranked_weighting.shape[0]),np.argmax(ranked_weighting, axis=1)][:,np.newaxis]
        ranked_weighting[ranked_weighting < self.thresh] = 0
        #TODO: rescale the rest of the values?
        for s in xrange(locale.shape[0]):
            ranked_result[s,:] = weighted_vote_result(predictions=self.predictions[locale[s,:]], weights=np.repeat(ranked_weighting[s,np.newaxis], repeats=self.predictions[locale[s,:]].shape[0], axis=0), classes=np.unique(X))
        
        ranked_performance = np.average(self.truth[locale] == ranked_result,axis=1)
        
        # Selections
        m_select = np.logical_and(individual_performance >= weighted_performance, individual_performance >= ranked_performance)
        m_rvote = np.logical_and(np.logical_not(m_select), ranked_performance >= weighted_performance)
        m_wvote = np.logical_and(np.logical_not(m_select), np.logical_not(m_rvote))
        
        prediction = np.zeros(shape=(locale.shape[0]))
        # Handle selection zones
        if np.any(m_select):
            s_pred = X[m_select, individual[m_select]]
            prediction[m_select] = s_pred
        
        # Handle voting zones
        if np.any(m_rvote):
            v_pred = weighted_vote_result(X[m_rvote,:], ranked_weighting[m_rvote,:], classes=np.unique(X))
            prediction[m_rvote] = v_pred
        
        # Handle voting zones
        if np.any(m_wvote):
            v_pred = weighted_vote_result(X[m_wvote,:], performance[m_wvote,:], classes=np.unique(X))
            prediction[m_wvote] = v_pred
            
        print 'Individual Distribution: ', np.bincount(individual[m_select])
        print 'Fusion Type Count: ', np.sum(m_select), np.sum(m_rvote), np.sum(m_wvote)
        self._run_data = {
            'individual': individual,
            'm_select': m_select,
            'm_rvote': m_rvote,
            'm_wvote': m_wvote
        }
        
        return prediction
    
    @property
    def individuals(self):
        return self._run_data['individual'], self._run_data['m_select']
    
    @property
    def fusion_masks(self):
        return dict([(m.split('_')[-1],self._run_data[m]) for m in ['m_select', 'm_rvote', 'm_wvote']])
    
    @property
    def fusion(self):
        return np.array([np.sum(self._run_data[m]) for m in ['m_select', 'm_rvote', 'm_wvote']])
    
    @property
    def predict_selected(self):
        ps = self._run_data['individual'].copy()
        ps[self._run_data['m_rvote']] = np.max(ps) + 1
        ps[self._run_data['m_wvote']] = np.max(ps) + 1
        
        return ps
    
#     def scores(self, locales):
#         X = np.zeros(shape=(locales.shape[0], len(self.cdf.primary)))
#         
#         p = self.predict(X, locales)
#         
#         
        
    
def weighted_vote_result(predictions, weights, classes=None):
    if classes == None:
        classes = np.unique(predictions)
    
    n_samples = predictions.shape[0]
    
    n_classifiers = predictions.shape[1]
    n_classes = classes.size
    
    '''Create a [n_samples, n_classifiers, n_classes] np.array and iterate
    over the classes, setting each cell along the classes axis as 1 if that
    sample/classifier combination picked it as the most likely class.
    '''
    votes = np.zeros((n_samples, n_classifiers, n_classes))
    for c in range(n_classes):
        votes[predictions == c, c] = 1
    
    # [n_samples, n_classes] matrix where M[i,j] is the weight of votes for class j for the ith sample
    weighted_votes = votes * weights[:,:,np.newaxis]
    weighted_vote = np.sum(weighted_votes, axis=1)
    
    return np.argmax(weighted_vote, axis=1)