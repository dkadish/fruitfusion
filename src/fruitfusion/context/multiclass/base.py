'''
Created on May 12, 2013

@author: dkadish
'''
from sklearn.base import BaseEstimator, TransformerMixin
from fruitfusion.context.multiclass import PrimaryClassifierSet
import numpy as np
from numpy.lib.utils import deprecate

class CDFBase(BaseEstimator):
    def __init__(self, primary_classifiers, **fit_params):
        self.primary = PrimaryClassifierSet(primary_classifiers)
        
        self.normalizer = NormalizerBase(self)
        self.locator = LocatorBase(self)
        self.segmenter = SegmenterBase(self)
        self.fuser = FuserBase(self)
    
    def fit(self, X, y=None, **fit_params):
        """Fit the contextual classifier. Determine the results of each primary
        classifier and then fit the contextual fusion classifier based on the
        results that only one primary classifier got correct.
        
        Parameters
        ==========
        X : dict of np.array
            A dictionary of feature sets containing features for each primary
            classifier and a contextual feature.
            
        y : np.array
        """
        assert set(self.primary.keys()) <= set(X.keys()), 'X must contain features for each primary classifier.'
        assert 'context' in X.keys(), 'X must contain contextual features.'
        
        # Set the possible classes
        self._classes = np.unique(y)
                
        # Get the contextual X
        X_ctx = self.normalizer.fit_transform(X['context'])
        
        # Set the contextual truth
        y_ctx = self.locator.locate(X, y, X_ctx) # dict = {classifier.id: np.array [n_samples,]} ]
        
        # Segment the space
        self.segmenter.fit(X_ctx, y_ctx)
        
        locale = self.segmenter.predict(X_ctx)
        
        # Determine fusion mechanisms for each locale
#         print 'Segments: ', self.locator.locale_sets
#         print 'COUNTS: ', np.bincount(y_ctx), np.bincount(locale)
#         print 'Segment Performance: ', np.average(y_ctx == locale)
        self.fuser.fit(X, y, locale)
    
    def fit_transform(self, X, y=None, **fit_params):
        """Fit all the transforms one after the other and transform the
        data, then use fit_transform on transformed data using the final
        estimator. Valid only if the final estimator implements
        fit_transform."""
        self.fit(X, y, **fit_params)
        return self.transform(X)
    
    def predict(self, X):
        """Applies transforms to the data, and the predict method of the
        final estimator. Valid only if the final estimator implements
        predict."""
        
        # Find the predictions of the primary classifiers
        predictions = self.primary.predict(X)
        
        # Find the locale
        X_ctx = self.normalizer.transform(X['context'])
        locale = self.segmenter.predict(X_ctx)
        
        # Determine the fusion mechanism to use
        #FIXME: this needs to work for a vector locale as well as a scalar
        return self.fuser.predict(predictions, locale)
    
#     @deprecate
#     def primary_predict(self, X):
#         '''Find the predictions from the primary classifiers.
#         
#         Parameters
#         ==========
#         X : dict { primary: np.array [n_samples, n_features]
#             A dictionary of the feature data arrays for each classifier
#         
#         Return
#         ======
#         predictions : np.array [n_samples, n_classifiers]
#         
#         DEPRICATED
#         '''
#         return self.primary.predict(X)

    def locale(self, X):
        X_ctx = self.normalizer.transform(X['context'])
        locale = self.segmenter.predict(X_ctx)
        
        return locale
    
    @property
    def locale_scores(self):
        return self._locale_scores
    
    def correct(self, predictions, y):
        return dict(map(lambda k: (k, predictions[k] == y), predictions))
    
    @property
    def classes(self):
        return self._classes
    
    @property
    def locales(self):
        #TODO: is this the smart way to do this?
        return self.segmenter.locales

class SegmenterBase(object):
    '''
    Methods
    =======
    fit
    
    predict
    '''
    
    def __init__(self, cdf):
        self.cdf = cdf
    
    @property
    def locales(self):
        pass
    
    def _calculate_shift(self):
        # Sets the bit shift
        shift = 0
        while 1 << shift < self.n_classes:
            shift += 1
        shift -= 1
        
        return shift
    
    def _calculate_bitshifted_y(self, X, y):
        # Finds the bitshifted y value for each sample
        y_bs = np.zeros(X.shape[0], dtype=np.int)
        for i, p in enumerate(y):

            y_bs += np.int64(y[p]) << self.shift*i
            
        return y_bs

class NormalizerBase(BaseEstimator,TransformerMixin):
    
    def fit(self):
        pass
    
    def transform(self):
        pass
    
    def fit_transform(self):
        pass
    
class LocatorBase(object):
    '''Base class for all locators.
    
    The locator class is responsible for determining the locale truth for an
    incoming set of features. This truth is the basis for the segmentation that
    occurs later on in the process.
    '''
    
    def __init__(self, cdf):
        self.cdf = cdf
        
    def locate(self, X, y, X_ctx):
        '''Locate must return locales as integers in the range of 1..n-1 ??
        '''
        pass
    
    def normalize_ctx(self, X_ctx):
        if hasattr(self.cdf, 'normalizer' ) and isinstance(self.cdf.normalizer, TransformerMixin):
            return self.cdf.normalizer.trans

class FuserBase(BaseEstimator):
    
    def __init__(self, cdf):
        self.cdf = cdf
    
    def fit(self, X, y, locales):
        pass
    
    def predict(self, X, locale):
        pass