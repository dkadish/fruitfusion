'''
Created on May 13, 2013

@author: dkadish
'''
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
import numpy as np

from fruitfusion.context.multiclass.base import SegmenterBase
from numpy.core.numeric import dtype
from sklearn.neighbors import BallTree
from fruitfusion.context.multiclass.locator import unique_rows
from scipy.stats.kde import gaussian_kde
from sklearn.tree.tree import DecisionTreeClassifier

class GlobalSegmenter(SegmenterBase):
    
    def fit(self, X, y):
        pass
    
    def predict(self, X):
        return np.zeros(X.shape[0], dtype=np.int8)
    
    @property
    def locales(self):
        return np.array([0])

class SVMMulti(SegmenterBase):
    '''Segments using a seperate SVM classifier for each primary classifier.
    
    Sort of depricated, but I want to keep the code around.
    '''
    def __init__(self, cdf):
        super(SVMMulti, self).__init__(cdf)
        self.segments = {}
        for p in self.cdf.primary:
            self.segments[p] = SVC()
                    
    def fit(self, X, y):
        # Assumes that all classes are labelled from 0..n-1
        for p in self.segments:
            self.segments[p].fit(X,y[p])
        
        self.classes = list(np.unique(np.concatenate(y.values())))
        self.n_classes = len(self.classes)
        
        self.shift = 0
        while 1 << self.shift < self.n_classes:
            self.shift += 1
            
        self.shift -= 1
            
    def predict(self, X):
        pred = {}
        code = 0
        for i, p in enumerate(self.segments):
            pred[p] = self.segments[p].predict(X)
            code += np.int64(pred[p]) << self.shift*i
        
        return code
        
    @property
    def locales(self):
        pass
    
class SVMSegmenter(SegmenterBase, SVC):
    '''Segments using a single SVM classifier.
    '''
    __DEFAULT_C = 5
    __DEFAULT_GAMMA = 0.05
    def __init__(self, cdf, C=None, gamma=None):
        SegmenterBase.__init__(self, cdf)
        SVC.__init__(self,kernel='rbf',
                     C=C != None and C or self.__DEFAULT_C,
                     gamma=gamma != None and gamma or self.__DEFAULT_GAMMA)
                    
    def fit(self, X, y):
        # Assumes that all classes are labelled from 0..n-1
        
#         self.classes = list(np.unique(np.concatenate(y.values())))
#         self.n_classes = len(self.classes)
        
        self._locales = np.unique(y)
            
        SVC.fit(self, X, y)
            
    @property
    def locales(self):
        return self._locales
    
class DTreeSegmenter(SegmenterBase, DecisionTreeClassifier):
    '''Segments using a single SVM classifier.
    '''
    __DEFAULT_CRITERION = 'gini'
    __DEFAULT_GAMMA = 0.05
    def __init__(self, cdf, criterion=None, max_depth=None):
        SegmenterBase.__init__(self, cdf)
        
        self.criterion = criterion == None and self.__DEFAULT_CRITERION or criterion
        self.max_depth = max_depth
        
        DecisionTreeClassifier.__init__(self, criterion=self.criterion, max_depth=self.max_depth)
    
    def fit(self, X, y):
        # Assumes that all classes are labelled from 0..n-1
        
#         self.classes = list(np.unique(np.concatenate(y.values())))
#         self.n_classes = len(self.classes)
        
        self._locales = np.unique(y)
            
        DecisionTreeClassifier.fit(self, X, y)
            
    @property
    def locales(self):
        return self._locales
    
class SVMBitshiftedSegmenter(SegmenterBase, SVC):
    '''Segments using a single SVM classifier.
    '''
    __DEFAULT_C = 5
    __DEFAULT_GAMMA = 0.05
    def __init__(self, cdf, C=None, gamma=None):
        SegmenterBase.__init__(self, cdf)
        SVC.__init__(self,kernel='linear',
                     C=C == None and self.__DEFAULT_C or C,
                     gamma=gamma == None and self.__DEFAULT_GAMMA or gamma)
                    
    def fit(self, X, y):
        # Assumes that all classes are labelled from 0..n-1
        
        self.classes = list(np.unique(np.concatenate(y.values())))
        self.n_classes = len(self.classes)
        
        self.shift = self._calculate_shift()
            
        y_bs = self._calculate_bitshifted_y(X, y)
            
        self._locales = np.unique(y_bs)
            
        SVC.fit(self, X, y_bs)
            
#     def predict(self, X):
#         pred = {}
#         code = 0
#         for i, p in enumerate(self.segments):
#             pred[p] = self.segments[p].predict(X)
#             code += np.int64(pred[p]) << self.shift*i
#         
#         return code
        
    @property
    def locales(self):
        return self._locales
    
class KNeighbourSegmenter(SegmenterBase):
    K_NEIGHBOURS = 10
    
    def __init__(self, cdf, k=None):
        SegmenterBase.__init__(self, cdf)
        self.k = k == None and self.K_NEIGHBOURS or k
    
    def fit(self, X, y=None):
        self.ball_tree = BallTree(X)
        
#         k_dists, k_inds = self.ball_tree.query(X, k=self.k)
        
#         k_inds = np.sort(k_inds, axis=1) # Get the closest points sorted
        
#         self._locales = unique_rows(k_inds)
        
    def predict(self, X):
        k_dists, k_inds = self.ball_tree.query(X, k=self.k)
        k_inds = np.sort(k_inds, axis=1) # Get the closest points sorted
        
        return k_inds
        
#     @property
#     def locales(self):
#         return self._locales

class GaussianKDESegmenter(SegmenterBase):
    
    def __init__(self, cdf, factor=0.8):
        SegmenterBase.__init__(self, cdf)
        self.n_classes = 3
        self.factor = factor
        
    def fit(self, X, y):
        ''' Expects X to be the context coordinates, and y[p] to be whether they are correct or not for primary classifier p. 
        '''
        self.positive_density = {}
        self.negative_density = {}
        self.cutoff = {}
        
        for p in self.cdf.primary:
            self.positive_density[p] = gaussian_kde(X[y[p]==1].T, bw_method=self.factor)
            self.negative_density[p] = gaussian_kde(X[y[p]==0].T, bw_method=self.factor)
            
#         densities = [self.density(p, X) for p in self.cdf.primary]
            
#         for p in self.cdf.primary:
#             d = np.exp(4*densities[p]) / dsum
# #             dmin, dmax = np.min(d), np.max(d)
# #             dmid = np.average([dmin, dmax])
# #             self.cutoff[p] = np.linspace(start=dmin, stop=dmax, num=3, endpoint=False)[1:]#[np.average([dmin,dmid]), np.average([dmid,dmax])] # At the 25% and 75% marks
#             self.cutoff[p] = [np.min(np.percentile(d, 50))]#, np.min(np.percentile(d, 50))]
        
    def predict(self, X):
        densities = [self.density(p, X) for p in self.cdf.primary]
#         dsum = np.exp(np.sum(densities, axis=0))
        
#         predict = {}
#         for p in self.cdf.primary:
# #             d = np.exp(4*densities[p]) / dsum
#             predict[p] = np.zeros(densities[p].shape)
# #             for i,c in enumerate(self.cutoff[p]):
# #                 predict[p][d>c] = i+1
#             for pj in self.cdf.primary:
#                 predict[p][densities[p] > densities[pj]] += 1
#         
#         # Convert to a code
#         self.shift = self._calculate_shift()
#         y = self._calculate_bitshifted_y(X, predict)
#         
#         self._locales = np.unique(y)
#         
#         return y
        
        self._locales = [p for p in self.cdf.primary]
        
        d = np.array(densities)
        
        return np.argmax(d, axis=0)
        
    def density(self, p, X):
        return self.positive_density[p](X.T) - self.negative_density[p](X.T)
        
    @property
    def locales(self):
        return self._locales