import UserDict
import collections
import numpy as np

class PrimaryClassifierSet(UserDict.IterableUserDict):
    def __init__(self, initialdata):
#         super(PrimaryClassifierSet, self).__init__()
        if isinstance(initialdata, collections.Mapping):
            self.data = dict(initialdata)
        elif isinstance(initialdata, collections.Iterable):
            self.data = dict(enumerate(initialdata))
        else:
            raise ValueError('The initial data submitted was not a valid Mapping or Iterable type.')
    
#     def __getattr__(self, name):    
#         if hasattr(UserDict.IterableUserDict, name):
#             return getattr(super(PrimaryClassifierSet, self), name)
#         elif hasattr(self.itervalues().next(), name) and callable(getattr(self.itervalues().next(), name)):
# #             print [self[i] for i in self]
#             print [getattr(self[i],name) for i in self]
# # #             print [getattr(self[i],name)() for i in self]
# # #             print [(i, getattr(self[i], name)()) for i in self]
# # #             print dict([(i, getattr(self[i], name)()) for i in self])
# #             fn = lambda *args, **kwargs: dict([(i, getattr(self[i], name)(args, kwargs)) for i in self])
# #             return fn
#         
#             def multi_func(*args, **kwargs):
#                 d = {}
#                 for i in self:
#                     print i, self[i], name, getattr(self[i], name), args, kwargs
#                     d[i] = getattr(self[i], name)(args, kwargs)
#                 return d
#             
#             return multi_func
#         else:
#             d = {}
#             for i in self:
#                 d[i] = getattr(self[i], name)
#             return d
        
    def predict(self, X):
        return dict([(p, self[p].predict(X[p])) for p in self])

def dict_to_array(d):
    '''
    If d is a dict of classifier_ids, then it returns an array in [n_??, n_classifiers]
    '''
    return np.array(d.values()).T