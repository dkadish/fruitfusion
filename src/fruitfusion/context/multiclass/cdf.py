'''
Created on May 13, 2013

@author: dkadish
'''
from fruitfusion.context.multiclass.base import CDFBase, LocatorBase,\
    SegmenterBase
from fruitfusion.context.multiclass import PrimaryClassifierSet
from fruitfusion.context.multiclass.segmenter import SVMBitshiftedSegmenter, KNeighbourSegmenter,\
    GaussianKDESegmenter, SVMSegmenter, GlobalSegmenter, DTreeSegmenter
from sklearn.preprocessing import StandardScaler
from fruitfusion.context.multiclass.locator import KNeighbourCorrectnessLocator,\
    RadiusNeighbourCorrectnessLocator,\
    WeightedRadiusNeighboursCorrectnessLocator, KNeighbourLocator,\
    SampleCorrectnessLocator, GaussianKDELocator,\
    WeightedRadiusNeighboursZoneLocator
from fruitfusion.context.multiclass.fuser import WeightedVoting, ClassifierRankSelection,\
    NeighboursClassifierRankSelection, SelectOrVoteFuser,\
    NeighbourSelectOrVoteFuser
    
import numpy as np

class KNeighbourSupportVectorWeightedVotingCDF(CDFBase):
    
    def __init__(self, primary_classifiers, n=None, C=None, gamma=None, **fit_params):
        self.primary = PrimaryClassifierSet(primary_classifiers)
        
        self.segmenter = SVMBitshiftedSegmenter(self, C, gamma)
        self.normalizer = StandardScaler()
        self.locator = KNeighbourCorrectnessLocator(self, n)
        self.fuser = WeightedVoting(self)
        
 
class RNeighbourSupportVectorWeightedVotingCDF(CDFBase):
    
    def __init__(self, primary_classifiers, **fit_params):
        self.primary = PrimaryClassifierSet(primary_classifiers)
        
        self.segmenter = SVMBitshiftedSegmenter(self)
        self.normalizer = StandardScaler()
        self.locator = RadiusNeighbourCorrectnessLocator(self)
        self.fuser = WeightedVoting(self)       
        
 
class WRNeighbourSupportVectorWeightedVotingCDF(CDFBase):
    
    def __init__(self, primary_classifiers, **fit_params):
        self.primary = PrimaryClassifierSet(primary_classifiers)
        
        self.segmenter = SVMBitshiftedSegmenter(self)
        self.normalizer = StandardScaler()
        self.locator = WeightedRadiusNeighboursCorrectnessLocator(self)
        self.fuser = WeightedVoting(self)    
 
# class KNeighbourSupportVectorSelector(CDFBase):
#     
#     def __init__(self, primary_classifiers, **fit_params):
#         self.primary = PrimaryClassifierSet(primary_classifiers)
#         
#         self.segmenter = SVM(self)
#         self.normalizer = StandardScaler()
#         self.locator = KNeighbourCorrectnessLocator(self)
#         self.fuser = WeightedVoting(self)

class DCSLA(CDFBase):
    '''
    Neighbourhoods are defined by the k-nearest-neighbours (i.e. zones where the nearest neigbours are the same).
    Segmentation is not a standard classification. This needs to be dealt with.
    Fusing just picks the most accurate classifier for the chosen class. (2.4 in Woods1997).
    '''
    
    def __init__(self, primary_classifiers, k=None, **fit_params):
        self.primary = PrimaryClassifierSet(primary_classifiers)
        
        self.normalizer = StandardScaler()
        self.locator = KNeighbourLocator(self)
        self.segmenter = KNeighbourSegmenter(self, k)
        self.fuser = NeighboursClassifierRankSelection(self)
        
    def scores(self, X_context):
        locale = self.segmenter.predict(X_context)
        return np.average(self.fuser._performance_matrix(locale), axis=1)

class GaussianKDELocalAccuracy(CDFBase):
    '''
    Neighbourhoods are defined by the k-nearest-neighbours (i.e. zones where the nearest neigbours are the same).
    Segmentation is not a standard classification. This needs to be dealt with.
    Fusing just picks the most accurate classifier for the chosen class. (2.4 in Woods1997).
    '''
    
    def __init__(self, primary_classifiers, k=None, **fit_params):
        self.primary = PrimaryClassifierSet(primary_classifiers)
        
        self.normalizer = StandardScaler()
        self.locator = SampleCorrectnessLocator(self)
        self.segmenter = GaussianKDESegmenter(self)
        self.fuser = ClassifierRankSelection(self)
    
    def scores(self, X_context):
        return np.array([self.segmenter.density(p, X_context) for p in self.primary]).T
    
class WeightedVote(CDFBase):
    def __init__(self, primary_classifiers, **fit_params):
        self.primary = PrimaryClassifierSet(primary_classifiers)
        
        self.normalizer = StandardScaler()
        self.locator = None
        self.segmenter = GlobalSegmenter(self)
        self.fuser = WeightedVoting(self)
        
    def fit(self, X, y=None, **fit_params):
        assert set(self.primary.keys()) <= set(X.keys()), 'X must contain features for each primary classifier.'
        assert 'context' in X.keys(), 'X must contain contextual features.'
        
        # Set the possible classes
        self._classes = np.unique(y)
                
        locale = np.zeros(X[0].shape[0], dtype=np.int8)
        
        # Determine fusion mechanisms for each locale
        self.fuser.fit(X, y, locale)
    
    def predict(self, X):
        # Find the predictions of the primary classifiers
        predictions = self.primary.predict(X)
        
        locale = np.zeros(X[0].shape[0], dtype=np.int8)
        
        # Determine the fusion mechanism to use
        return self.fuser.predict(predictions, locale)

class KdeSvmSelectOrVoteCDF(CDFBase):
    '''
    Neighbourhoods are defined by the k-nearest-neighbours (i.e. zones where the nearest neigbours are the same).
    Segmentation is not a standard classification. This needs to be dealt with.
    Fusing just picks the most accurate classifier for the chosen class. (2.4 in Woods1997).
    '''
    
    def __init__(self, primary_classifiers, factor=None, thresh=None, C=None, gamma=None, **fit_params):
        self.primary = PrimaryClassifierSet(primary_classifiers)
        
        self.normalizer = StandardScaler()
        self.locator = GaussianKDELocator(self, factor=factor, thresh=thresh)
        self.segmenter = SVMSegmenter(self, C, gamma)
        self.fuser = SelectOrVoteFuser(self)

class KdeDTreeSelectOrVoteCDF(CDFBase):
    '''
    Neighbourhoods are defined by the k-nearest-neighbours (i.e. zones where the nearest neigbours are the same).
    Segmentation is not a standard classification. This needs to be dealt with.
    Fusing just picks the most accurate classifier for the chosen class. (2.4 in Woods1997).
    '''
    
    def __init__(self, primary_classifiers, factor=None, thresh=None, criterion=None, max_depth=None, **fit_params):
        self.primary = PrimaryClassifierSet(primary_classifiers)
        
        self.normalizer = StandardScaler()
        self.locator = GaussianKDELocator(self, factor=factor, thresh=thresh)
        self.segmenter = DTreeSegmenter(self, criterion=criterion, max_depth=max_depth)
        self.fuser = SelectOrVoteFuser(self)
        
class KNeighbourSvmSelectOrVote(CDFBase):
    def __init__(self, primary_classifiers, n=None, k=None, C=None, gamma=None, **fit_params):
        self.primary = PrimaryClassifierSet(primary_classifiers)
        
        self.segmenter = SVMBitshiftedSegmenter(self, C, gamma)
        self.normalizer = StandardScaler()
        self.locator = KNeighbourCorrectnessLocator(self, n=n, k=k)
        self.fuser = SelectOrVoteFuser(self)
        
class KNeighbourSelectOrVote(CDFBase):
    def __init__(self, primary_classifiers, k=None, thresh=None, **fit_params):
        self.primary = PrimaryClassifierSet(primary_classifiers)
        
        self.normalizer = StandardScaler()
        self.locator = KNeighbourLocator(self)
        self.segmenter = KNeighbourSegmenter(self, k)
        self.fuser = NeighbourSelectOrVoteFuser(self, thresh)
        
    def scores(self, X_ctx):
        locales = self.segmenter.predict(X_ctx)
        return self.fuser.scores(locales)
        
class RNeighbourSvmSelectOrVoteCDF(CDFBase):
    '''
    Neighbourhoods are defined by the k-nearest-neighbours (i.e. zones where the nearest neigbours are the same).
    Segmentation is not a standard classification. This needs to be dealt with.
    Fusing just picks the most accurate classifier for the chosen class. (2.4 in Woods1997).
    '''
    
    def __init__(self, primary_classifiers, r=None, thresh=None, C=None, gamma=None, **fit_params):
        self.primary = PrimaryClassifierSet(primary_classifiers)
        
        self.normalizer = StandardScaler()
        self.locator = WeightedRadiusNeighboursZoneLocator(self, r, thresh)
        self.segmenter = SVMSegmenter(self, C, gamma)
        self.fuser = SelectOrVoteFuser(self)