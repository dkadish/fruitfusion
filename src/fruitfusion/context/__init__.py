'''
'''
from sklearn.base import ClassifierMixin
from sklearn.neighbors.base import NeighborsBase, KNeighborsMixin,\
    SupervisedIntegerMixin, _check_weights
    
import numpy as np
from sklearn.neighbors.classification import KNeighborsClassifier,\
    RadiusNeighborsClassifier
from timeit import itertools
from sklearn.svm.classes import SVC

class BaseCDF(ClassifierMixin):
    def __init__(self, primary_classifiers, **fit_params):
        self.primary = primary_classifiers
    
    def fit(self, X, y=None, **fit_params):
        """Fit the contextual classifier. Determine the results of each primary
        classifier and then fit the contextual fusion classifier based on the
        results that only one primary classifier got correct.
        
        Parameters
        ==========
        X : dict of np.array
            A dictionary of feature sets containing features for each primary
            classifier and a contextual feature.
            
        y : np.array
            
            
        """
        assert set(self.primary.keys()) <= set(X.keys()), 'X must contain features for each primary classifier.'
        assert 'context' in X.keys(), 'X must contain contextual features.'
        
        X_ctx = X['context']
        y_ctx = self.context_truth(self.primary_predictions(X), y)
        
        # Makes it easier to ignore the 'both' and 'neither'
        any = y_ctx >= 0
        
        self.context.fit(X_ctx[any], y_ctx[any], **fit_params)
        
        return self
    
    def fit_transform(self, X, y=None, **fit_params):
        """Fit all the transforms one after the other and transform the
        data, then use fit_transform on transformed data using the final
        estimator. Valid only if the final estimator implements
        fit_transform."""
        self.fit(X, y, **fit_params)
        return self.transform(X)
    
    def predict(self, X):
        """Applies transforms to the data, and the predict method of the
        final estimator. Valid only if the final estimator implements
        predict."""
        assert set(self.primary.keys()) <= set(X.keys()), 'X must contain features for each primary classifier.'
        assert 'context' in X.keys(), 'X must contain contextual features.'

        predictions = np.array(map(lambda c: self.primary[c].predict(X[c]), self.primary)).T
        
        equal = [np.equal(predictions[:,i], predictions[:,i+1]) for i in xrange(predictions.shape[1]-1)]
        agree = reduce(lambda a,b: np.logical_and(a,b), equal, True)
        disagree = np.logical_not(agree)
        
        which = np.int8(self.context.predict(X['context'][disagree]))
        
        fused_prediction = np.zeros(predictions[:,0].shape)
        fused_prediction -= 1
        fused_prediction[agree] = predictions[agree,0]
        fused_prediction[disagree] = predictions[disagree,which]
        
        assert (fused_prediction >= 0).all(), 'Some predictions not fused'
        
        return fused_prediction
    
    def primary_predictions(self, X):
        return dict((map(lambda c: (c, self.primary[c].predict(X[c])), self.primary)))
    
    def context_truth(self, predictions, truth):
        '''Generates a set of y-values for context classification based on
        the results of primary classification.
        
        The y-values represent the index of the correct classifier when
        iterating over self.primary.
        '''
        # Generate a dict of bools representing the validity of predictions
        correct = self.correct(predictions, truth)
        
        # Numpy array that is True when either (but not both) classifier is correct. THIS WILL NOT WORK FOR MORE THAN 2 CLASSIFIERS!
        either = self.either(correct)
        
        # A dict that is True when that classifier (and only that classifier) is correct
        which = self.which(either, correct)
        
        # Numpy array that is True when neither classifier is correct
        neither = self.neither(correct)
        
        self.primary_results = {
            'correct': correct,
            'either': either,
            'which': which,
            'neither': neither
        }
        assert reduce(lambda a,b: a and b, map(lambda a: a.shape == either.shape, itertools.chain(correct.values(), [either], which.values(), [neither])), True), 'Arrays should be the same shape'
        
        print 'Both Right: ', np.sum(self.both(correct)), 'One Right: ', np.sum(either), 'None Right: ', np.sum(neither)
        
        # Create the new y array
        y = np.zeros(predictions[0].shape)
        y -= 1
        for i,k in enumerate(self.primary):
            y[which[k]] = i
        y[neither] = -2
        
        assert y[which[self.primary.keys()[1]]].shape == y[y==1].shape, 'Shape of the correct classifiers should match (%s != %s).' %(str(y[which[self.primary.keys()[0]]].shape),str(y[y==0].shape))
        
        return y
    
    # Functions to compare answers #FIXME
    def correct(self, predictions, y):
        return dict(map(lambda k: (k, predictions[k] == y), predictions))
    
    def both(self, correct):
        return reduce(lambda a,b: np.logical_and(a,b), correct.values())
    
    def either(self, correct):
        return reduce(lambda a,b: np.logical_xor(a,b), correct.values())
    
    def which(self, either, correct):
        return dict(map(lambda f: (f, np.logical_and(either, correct[f])), correct))
    
    def neither(self, correct):
        return reduce(lambda a,b: np.logical_not(np.logical_or(a,b)), correct.values())

class SupportVectorCDF(BaseCDF):
    
    def __init__(self, primary_classifiers,
                 C=1.0, kernel='rbf', degree=3, gamma=0.0, coef0=0.0,
                 shrinking=True, probability=False, tol=0.001, cache_size=200,
                 class_weight=None, verbose=False, max_iter=-1):
        
        super(SupportVectorCDF, self).__init__(primary_classifiers)
        
        self.context = SVC(C, kernel, degree, gamma, coef0, shrinking, probability, tol, cache_size, class_weight, verbose)

class RadiusNeighboursCDF(BaseCDF):
    
    def __init__(self, primary_classifiers,
                 radius=1.0, weights='uniform', algorithm='auto', leaf_size=30,
                 p=2, outlier_label=None):
        
        super(RadiusNeighboursCDF, self).__init__(primary_classifiers)
        
        self.context = RadiusNeighborsClassifier(radius, weights, algorithm, leaf_size, p, outlier_label)
        
class NearestNeighboursCDF(BaseCDF):
    """Context dependent fusion using nearest neighbours context classification.

    Implements a context dependent fusion system based on the work of Gyeongyong Heo
    (Robust kernel methods in context-dependent fusion, 2009).
    
    The standard sklearn functions (fit, transform, etc.) work on the entire
    pipeline of data, taking in the initial feature sets (as a dict of
    features) and returning the final class assignments. The contextual
    classifier can be accessed through the 'context' variable and the primary
    classifiers through the 'primary' variable.

    Parameters
    ----------
    primary : dict
        Dictionary in the format { name: clf } where clf implements predict(X)
    
    n_neighbors : int, optional (default = 5)
        Number of neighbors to use by default for :meth:`k_neighbors` queries.

    weights : str or callable
        weight function used in prediction.  Possible values:

        - 'uniform' : uniform weights.  All points in each neighborhood
          are weighted equally.
        - 'distance' : weight points by the inverse of their distance.
          in this case, closer neighbors of a query point will have a
          greater influence than neighbors which are further away.
        - [callable] : a user-defined function which accepts an
          array of distances, and returns an array of the same shape
          containing the weights.

        Uniform weights are used by default.

    algorithm : {'auto', 'ball_tree', 'kd_tree', 'brute'}, optional
        Algorithm used to compute the nearest neighbors:

        - 'ball_tree' will use :class:`BallTree`
        - 'kd_tree' will use :class:`scipy.spatial.cKDtree`
        - 'brute' will use a brute-force search.
        - 'auto' will attempt to decide the most appropriate algorithm
          based on the values passed to :meth:`fit` method.

        Note: fitting on sparse input will override the setting of
        this parameter, using brute force.

    leaf_size : int, optional (default = 30)
        Leaf size passed to BallTree or cKDTree.  This can affect the
        speed of the construction and query, as well as the memory
        required to store the tree.  The optimal value depends on the
        nature of the problem.

    warn_on_equidistant : boolean, optional.  Defaults to True.
        Generate a warning if equidistant neighbors are discarded.
        For classification or regression based on k-neighbors, if
        neighbor k and neighbor k+1 have identical distances but
        different labels, then the result will be dependent on the
        ordering of the training data.
        If the fit method is ``'kd_tree'``, no warnings will be generated.

    p: integer, optional (default = 2)
        Parameter for the Minkowski metric from
        sklearn.metrics.pairwise.pairwise_distances. When p = 1, this is
        equivalent to using manhattan_distance (l1), and euclidean_distance
        (l2) for p = 2. For arbitrary p, minkowski_distance (l_p) is used.


    Examples
    --------
    >>> X = [[0], [1], [2], [3]]
    >>> y = [0, 0, 1, 1]
    >>> from sklearn.neighbors import KNeighborsClassifier
    >>> neigh = KNeighborsClassifier(n_neighbors=3)
    >>> neigh.fit(X, y) # doctest: +ELLIPSIS
    KNeighborsClassifier(...)
    >>> print(neigh.predict([[1.1]]))
    [0]
    >>> print(neigh.predict_proba([[0.9]]))
    [[ 0.66666667  0.33333333]]

    See also
    --------
    RadiusNeighborsClassifier
    KNeighborsRegressor
    RadiusNeighborsRegressor
    NearestNeighbors

    Notes
    -----
    See :ref:`Nearest Neighbors <neighbors>` in the online documentation
    for a discussion of the choice of ``algorithm`` and ``leaf_size``.

    http://en.wikipedia.org/wiki/K-nearest_neighbor_algorithm

    """
    
    def __init__(self, primary_classifiers,
                 n_neighbors=5, weights='uniform',
                 algorithm='auto', leaf_size=30,
                 warn_on_equidistant=True, p=2):
        super(NearestNeighboursCDF, self).__init__(primary_classifiers)
        
        # From kNN
        self.context = KNeighborsClassifier(n_neighbors, weights, algorithm, leaf_size, warn_on_equidistant, p)
            
    #===========================================================================
    # def fit(self, X, y=None, **fit_params):
    #    """Fit the contextual classifier. Determine the results of each primary
    #    classifier and then fit the contextual fusion classifier based on the
    #    results that only one primary classifier got correct.
    #    
    #    Parameters
    #    ==========
    #    X : dict of np.array
    #        A dictionary of feature sets containing features for each primary
    #        classifier and a contextual feature.
    #        
    #    y : np.array
    #        
    #        
    #    """
    #    assert set(self.primary.keys()) <= set(X.keys()), 'X must contain features for each primary classifier.'
    #    assert 'context' in X.keys(), 'X must contain contextual features.'
    #    
    #    X_ctx = X['context']
    #    y_ctx = self.context_truth(self.primary_predictions(X), y)
    #    
    #    # Makes it easier to ignore the 'both' and 'neither'
    #    any = y_ctx >= 0
    #    
    #    self.context.fit(X_ctx[any], y_ctx[any], **fit_params)
    #    
    #    return self
    #===========================================================================

    #===========================================================================
    # def fit_transform(self, X, y=None, **fit_params):
    #    """Fit all the transforms one after the other and transform the
    #    data, then use fit_transform on transformed data using the final
    #    estimator. Valid only if the final estimator implements
    #    fit_transform."""
    #    self.fit(X, y, **fit_params)
    #    return self.transform(X)
    #===========================================================================

#===============================================================================
#    def predict(self, X):
#        """Applies transforms to the data, and the predict method of the
#        final estimator. Valid only if the final estimator implements
#        predict."""
#        assert set(self.primary.keys()) <= set(X.keys()), 'X must contain features for each primary classifier.'
#        assert 'context' in X.keys(), 'X must contain contextual features.'
# #        predictions = {}
# #        for p in self.primary:
# #            predictions[p] = self.primary[p].predict(X[p])
#        predictions = np.array(map(lambda c: self.primary[c].predict(X[c]), self.primary)).T
#        
#        equal = [np.equal(predictions[:,i], predictions[:,i+1]) for i in xrange(predictions.shape[1]-1)]
#        agree = reduce(lambda a,b: np.logical_and(a,b), equal, True)
#        disagree = np.logical_not(agree)
#        
#        which = self.context.predict(X['context'][disagree])
#        
#        fused_prediction = np.zeros(predictions[:,0].shape)
#        fused_prediction -= 1
#        fused_prediction[agree] = predictions[agree,0]
#        fused_prediction[disagree] = predictions[disagree,which]
#        
#        assert (fused_prediction >= 0).all(), 'Some predictions not fused'
#        
#        return fused_prediction
#===============================================================================
    
    def predict_proba(self, X):
        """Applies transforms to the data, and the predict_proba method of the
        final estimator. Valid only if the final estimator implements
        predict_proba."""
        #TODO: Implement this
        pass

#    def decision_function(self, X):
#        """Applies transforms to the data, and the decision_function method of
#        the final estimator. Valid only if the final estimator implements
#        decision_function."""
#        Xt = X
#        for name, transform in self.steps[:-1]:
#            Xt = transform.transform(Xt)
#        return self.steps[-1][-1].decision_function(Xt)

    def predict_log_proba(self, X):
        pass
    
    def transform(self, X):
        """Applies transforms to the data, and the transform method of the
        final estimator. Valid only if the final estimator implements
        transform."""
        pass
    
    def inverse_transform(self, X):
        pass

    def score(self, X, y=None):
        """Applies transforms to the data, and the score method of the
        final estimator. Valid only if the final estimator implements
        score."""
        super(NearestNeighboursCDF, self)
    
    #===========================================================================
    # def primary_predictions(self, X):
    #    return dict((map(lambda c: (c, self.primary[c].predict(X[c])), self.primary)))
    #===========================================================================
    
    #===========================================================================
    # def context_truth(self, predictions, truth):
    #    '''Generates a set of y-values for context classification based on
    #    the results of primary classification.
    #    
    #    The y-values represent the index of the correct classifier when
    #    iterating over self.primary.
    #    '''
    #    # Generate a dict of bools representing the validity of predictions
    #    correct = self.correct(predictions, truth)
    #    
    #    # Numpy array that is True when either (but not both) classifier is correct. THIS WILL NOT WORK FOR MORE THAN 2 CLASSIFIERS!
    #    either = self.either(correct)
    #    
    #    # A dict that is True when that classifier (and only that classifier) is correct
    #    which = self.which(either, correct)
    #    
    #    # Numpy array that is True when neither classifier is correct
    #    neither = self.neither(correct)
    #    
    #    self.primary_results = {
    #        'correct': correct,
    #        'either': either,
    #        'which': which,
    #        'neither': neither
    #    }
    #    assert reduce(lambda a,b: a and b, map(lambda a: a.shape == either.shape, itertools.chain(correct.values(), [either], which.values(), [neither])), True), 'Arrays should be the same shape'
    #    
    #    print 'Both Right: ', np.sum(self.both(correct)), 'One Right: ', np.sum(either), 'None Right: ', np.sum(neither)
    #    
    #    # Create the new y array
    #    y = np.zeros(predictions[0].shape)
    #    y -= 1
    #    for i,k in enumerate(self.primary):
    #        y[which[k]] = i
    #    y[neither] = -2
    #    
    #    assert y[which[self.primary.keys()[1]]].shape == y[y==1].shape, 'Shape of the correct classifiers should match (%s != %s).' %(str(y[which[self.primary.keys()[0]]].shape),str(y[y==0].shape))
    #    
    #    return y
    #===========================================================================
    
    #===========================================================================
    # # Functions to compare answers #FIXME
    # def correct(self, predictions, y):
    #    return dict(map(lambda k: (k, predictions[k] == y), predictions))
    # 
    # def both(self, correct):
    #    return reduce(lambda a,b: np.logical_and(a,b), correct.values())
    # 
    # def either(self, correct):
    #    return reduce(lambda a,b: np.logical_xor(a,b), correct.values())
    # 
    # def which(self, either, correct):
    #    return dict(map(lambda f: (f, np.logical_and(either, correct[f])), correct))
    # 
    # def neither(self, correct):
    #    return reduce(lambda a,b: np.logical_not(np.logical_or(a,b)), correct.values())
    #===========================================================================
