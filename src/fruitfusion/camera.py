'''
Created on Mar 5, 2013

@author: dkadish

@TODO: Colour balance.
'''
import numpy as np
from skimage.io import imread, imsave
from tools import plot_img
import scipy
import scipy.fftpack, scipy.ndimage
import os
from sklearn.externals import joblib
import random

rand = random.SystemRandom()

class CameraEffect(object):
    
    def __init__(self):
        pass
    
    def process(self, img):
        pass
    
    def process_and_crop(self):
        pass
    
    def crop_bounds(self):
        return np.array([[0,0],[0,0]])

class PointSpreadFunction(CameraEffect):
    pass

class AperatureAdaptation(CameraEffect):
    
    def __init__(self):
        pass
    
    def process(self, img):
        c = None # constant
        t = None # exposure time
        s = None # film ISO
        b = None # f-number
        
        return img * c * t * s / (b**2)
        
class StructuralBlur(CameraEffect):
    
    def __init__(self, size):
        #self.size = 10#size
        #self.intensity = intensity
        
        self.sigma_x = float(size)#1.0
        self.sigma_y = float(size)#1.0
        self.size = int(10*size)+1
        
    def process(self, img):
        x = np.arange(-self.size/2, self.size/2) * np.ones((self.size,self.size))
        y = (np.arange(-self.size/2, self.size/2) * np.ones((self.size,self.size))).T
        kernel = np.exp(-np.power(x/2,2)/(2*self.sigma_x**2) - np.power(y/2,2)/(2*self.sigma_y**2))
        kernel /= np.sum(kernel)
        
        # padded fourier transform, with the same shape as the image
        kernel_ft = scipy.fftpack.fft2(kernel, shape=img.shape[:2], axes=(0, 1))
        
        # convolve
        img_ft = scipy.fftpack.fft2(img, axes=(0, 1))
        img2_ft = kernel_ft[:, :, np.newaxis] * img_ft
        img2 = scipy.fftpack.ifft2(img2_ft, axes=(0, 1)).real
        
        # clip values to range
        img2 = np.clip(img2, 0, 1)
        
        return img2
    
    def process_and_crop(self, img):
        im = self.process(img)
        
        return im[self.size:-self.size, self.size:-self.size]
        
    def crop_bounds(self):
        b = int(round(self.size))
        return np.array([[b,-b], [b,-b]])
    
    @property
    def params(self):
        return {
            'sigma_x': self.sigma_x,
            'sigma_y': self.sigma_y,
            'size': self.size,
        }

class MotionBlur(CameraEffect):
    
    def __init__(self, length, angle):
        self.length = length + 1
        self.angle = angle # in degrees
    
    def process(self, img):
        kernel = np.zeros((self.length, self.length))
        kernel[(kernel.shape[0]/2):(kernel.shape[0]/2+1), 0:self.length] = 1
        kernel /= np.sum(kernel)
        kernel = scipy.ndimage.rotate(kernel, self.angle)
        
        kernel_ft = scipy.fftpack.fft2(kernel, shape=img.shape[:2], axes=(0, 1))
        img_ft = scipy.fftpack.fft2(img, axes=(0, 1))
        
        img2_ft = kernel_ft[:, :, np.newaxis] * img_ft
        img2 = scipy.fftpack.ifft2(img2_ft, axes=(0, 1)).real
        img2 = np.clip(img2, 0, 1)
        
        return img2
    
    def process_and_crop(self, img):
        im = self.process(img)
        
        return im[self.length:-self.length, self.length:-self.length]
        
    def crop_bounds(self):
        b = int(round(self.length))
        return np.array([[b,-b], [b,-b]])
    
    @property
    def params(self):
        return {
            'length': self.length,
            'angle': self.angle,
        }

class SensorNoise(CameraEffect):
    
    def __init__(self, threshold):
        
        self.threshold = threshold
    
    def process(self, img):
        noise = np.random.normal(loc=0.0, scale=1.0, size=img.shape) #* img
        mask = np.random.exponential(scale=1.0, size=img.shape)
        noise[mask<self.threshold] = 0.0
        
        img2 = np.clip(img + noise, 0, 1)
        
        return img2
    
    def process_and_clip(self, img): return self.process(img)
    
    @property
    def params(self):
        return {
            'threshold': self.threshold,
        }

class CameraPipeline(CameraEffect):
    
    def __init__(self, steps=[]):
        self.steps = steps
        
    def add_step(self, name, step):
        self.steps.append((name,step))
        
    def process(self, img):
        if self.steps != []:
            for name, step in self.steps:
                img = step.process(img)
        
        return img
    
    def process_and_crop(self, img):
        img = self.process(img)
        b = self.crop_bounds()
#        print b, img[b[0,0]:(b[0,1] != 0 and b[0,1] or None), b[1,0]:(b[1,1] != 0 and b[1,1] or None)].shape
        
        return img[b[0,0]:(b[0,1] != 0 and b[0,1] or None), b[1,0]:(b[1,1] != 0 and b[1,1] or None)]
    
    def crop_bounds(self):
        if self.steps != []:
            b = np.amax(np.dstack([np.abs(step.crop_bounds()) for name,step in self.steps]), axis=2)
            b[:,1] = -b[:,1]
            return b
        else:
            return np.array([[0,0],[0,0]])

def _visualize_main(args):
    if args.operation == 'aperature':
        pass
    elif args.operation == 'blur':
        sim = StructuralBlur(2)#, 30)
    elif args.operation == 'motion':
        sim = MotionBlur(20, 25.0)
    elif args.operation == 'noise':
        sim = SensorNoise(2.0)
        
    img = imread(args.img)
    img = np.float_(img)/255.0
    
    processed = sim.process(img)
    
    plot_img(img)
    plot_img(processed)
    
def _load(f, images, dest):
    try:
        img = np.float64(imread(images[f]))/255.0
        pipeline = CameraPipeline([])
            
        # Blur
        r = rand.random()
        if r < 0.15:
            pipeline.add_step('blur', StructuralBlur(rand.random()*2))
            
        # Motion Blu
        r = rand.random()
        if r < 0.15:
            pipeline.add_step('motion', MotionBlur(rand.random()*20, rand.random()*25.0))
        
        # Noise
        r = rand.random()
        if r < 0.3:
            pipeline.add_step('noise', SensorNoise(rand.normalvariate(2.5, 1.0)))
            
#        r = random.random()
#        if r < 0.2:
#            img = darken(img)
#        elif r < 0.4:
#            img = lighten(img)

        img = pipeline.process_and_crop(img)
        
        print '%s: %s' %(f, pipeline.steps != [] and ' | '.join([str(s.params) for n,s in pipeline.steps]) or '')
        
        img = np.clip(img, 0, 1)
        
    except IOError:
        print 'Could not process image %s' %images[f]
        
    dir = os.path.split(os.path.join(dest,f))[0]
    if not os.path.exists(dir):
        os.makedirs(dir)

    imsave(os.path.join(dest,f), img)
    
def _process_main(args):
    src = args.input
    dest = args.output
    
    images = {}
    for r,d,f in os.walk(src):
        for fi in filter(lambda a: os.path.splitext(a)[-1] == '.jpg', f):
            images[os.path.relpath(os.path.join(r,fi), src)] = os.path.join(r,fi)
    
    joblib.Parallel(n_jobs=6, pre_dispatch='2*n_jobs', verbose=1)(joblib.delayed(_load)(i, images, dest) for i in images)
#    for i in images:
#        _load(i, images, dest)

if __name__=='__main__':
    '''Example:
    python colours.py -i ~/documents/school/grad/projects/ai/tropical-fruits/agata_potato/agata_potato_016.jpg hist
    '''
    import argparse
    
    parser = argparse.ArgumentParser()
    
    subparsers = parser.add_subparsers(help='commands')
    visualize_parser = subparsers.add_parser('visualize', help='Visualize the image transformations.')
    visualize_parser.add_argument('-i', dest='img', help='Input image file')
    visualize_parser.add_argument('-o', dest='output', help='Where to output the image file')
    visualize_parser.add_argument(dest='operation', help='Which operation to perform')
    visualize_parser.set_defaults(func=_visualize_main)
    
    process_parser = subparsers.add_parser('process', help='')
    process_parser.add_argument('-i', dest='input', help='Folder with the images')
    process_parser.add_argument('-o', dest='output', help='Where to output the image file')
    process_parser.set_defaults(func=_process_main)
    
    args = parser.parse_args()
    args.func(args)
