'''
Created on Mar 1, 2013

@author: dkadish

A module containing features that return shape-based descriptors for input images.
'''
import sys, os
sys.path.append(os.path.abspath(os.path.join(__file__,'../../')))
from tools import plot_img

import math

import numpy as np

from skimage.filter import hsobel, vsobel
from skimage.io import imread

def edge_orientation_autocorrelogram(img, scale_normalization=True):
    '''
    Based on Mahmoudi (2003) "Image retrieval based on shape similarity by edge orientation autocorrelogram"
    '''
    # Step 1: Edge Detection
    G_x = hsobel(img)
#    plot_img(G_x)
    G_y = vsobel(img)
#    plot_img(G_y)
    
    G_mag = np.sqrt(G_x ** 2 + G_y ** 2)
#    plot_img(G_mag)
    G_ang = np.arctan2(G_y,G_x)
#    plot_img(G_ang)
    
    # Step 2: Finding prominent edges
    threshold  = 0.1 # 10% of the max
    i_thresh = G_mag > threshold 
#    plot_img(i_thresh)
    #G_ang[G_mag < threshold] = 0
    
    # Step 3: Edge orientation quantization
    ''' N.B. Change from the original
    The original algorithm goes from 0-360, with 36 quanta. This makes no sense.
    In the np.arctan2, with the results from Sobel, you'll never get more than
    180 degrees. So we stop just after pi with 18 quanta.
    '''
    G_ang_quants = []
    quanta = np.arange(0,math.pi/2,math.pi/2/18)
    for i in xrange(quanta.size-1):
        in_quanta = np.logical_and(G_ang >= quanta[i], G_ang < quanta[i+1])
        in_quanta_thresh = np.logical_and(in_quanta, i_thresh)
        G_ang_quants.append(in_quanta_thresh)
#        if np.sum(in_quanta_thresh) > 100: plot_img(in_quanta_thresh)
    # Make sure the last quanta gets all the way to the end!
    assert np.max(G_ang) <= math.pi/2, 'All angle values should be smaller than pi/2'
    in_quanta_thresh = np.logical_and(G_ang >= quanta[-1], i_thresh)
#    if np.sum(in_quanta_thresh) > 100: plot_img(in_quanta_thresh)
    G_ang_quants.append(in_quanta_thresh)
    
    # Step 4: Determine the distance set
    # Really create a mask to mask around each point
    D = []
    for d in [1,3,5,7]:
        mask = np.ones((d*2+1,d*2+1), dtype=bool)
        mask[1:-1,1:-1] = False
        D.append(mask)
#    max_dist = 7
#    mask = np.ones((max_dist*2+1,max_dist*2+1), dtype=bool)
#    for i in xrange(max_dist+1):
#        mask[i:-i,i:-i] = i%2 == 0 
    
    # Step 5: Computing the elements of the EOAC
    eoac = np.zeros((len(G_ang_quants),len(D)))
    G_ang_i = np.indices(G_ang.shape)
    for j,q in enumerate(G_ang_quants):
        ind = G_ang_i[:,q]
        for i in xrange(ind.shape[1]):
            for k,m in enumerate(D):
                x,y = ind[:,i]
                
                # Make sure we aren't overlapping the sides
                x_offset = (m.shape[0]-1)/2
                x_low, x_high = x - x_offset, x + x_offset + 1
                y_offset = (m.shape[1]-1)/2
                y_low, y_high = y - y_offset, y + y_offset + 1
                
                if x_low < 0:
                    m = m[-x_low:,:]
                    x_low = 0
                if y_low < 0:
                    m = m[:,-y_low:]
                    y_low = 0
                if x_high > q.shape[0]:
                    m = m[:q.shape[0]-x_high,:] # Should this be -x_high + 1?
                    x_high = q.shape[0]
                if y_high > q.shape[1]:
                    m = m[:,:q.shape[1]-y_high] # Should this be -y_high + 1?
                    y_high = q.shape[1]
                
#                print q.shape, x_low, x_high, y_low, y_high, m
                slice = q[x_low:x_high, y_low:y_high][m]
                
                eoac[j,k] += np.sum(slice)
                
    if scale_normalization:
        # This will return a float instead of an int
        total = np.sum(eoac)
        if total != 0:
            eoac = np.float_(eoac) / total
        else:
            eoac = np.zeros(eoac.shape, dtype=float)
    
    return np.reshape(eoac,(-1,1))

def _main_eoac(args):
    img = imread(args.img, as_grey=True)
    eoac = edge_orientation_autocorrelogram(img)
    print eoac

if __name__=='__main__':
    '''Example:
    python colours.py -i ~/documents/school/grad/projects/ai/tropical-fruits/agata_potato/agata_potato_016.jpg hist
    '''
    import argparse
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-i', dest='img', help='Input image file')
#    parser.add_argument('-o', dest='output', help='Where to output the image file')
    
    subparsers = parser.add_subparsers(help='commands')
    eoac_parser = subparsers.add_parser('eoac', help='Extracts eoac from images.')
    eoac_parser.set_defaults(func=_main_eoac)
    
    args = parser.parse_args()
    args.func(args)