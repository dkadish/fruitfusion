''' Image Preprocessing functions based on ImageMagick
'''
import subprocess
import numpy as np
from sklearn.externals.joblib import Parallel, delayed, dump
from itertools import izip
import os
import random

def noise(img, out, amount, density=100, blur=0):
    args = ['filmgrain', '-a', str(amount), '-A', '0', '-d', str(density), '-b', '0', '-s', str(int(random.random()*10000)), img, out]
    subprocess.call(args)
    
    return os.path.split(img)[-1], amount

def colour(img, out, temperature):
    args = ['colortemp', '-t', str(temperature), img, out]
    subprocess.call(args)
    
    return os.path.split(img)[-1], temperature 

def exposure(img, out, amount):
    args = ['exposure', '-a', str(amount), img, out]
    subprocess.call(args)
    
    return os.path.split(img)[-1], amount 

def focusblur(img, out, amount):
    args = ['camerablur', '-t', 'defocus', '-a', str(amount), img, out]
    subprocess.call(args)
    
    return os.path.split(img)[-1], amount

def motionblur(img, out, amount):
    args = ['camerablur', '-t', 'motion', '-a', str(amount), img, out]
    subprocess.call(args)
    
    return os.path.split(img)[-1], amount

def all(img, out, noise_max=300, blur_max=40.0):
    args = []
    saveargs = []
    
    amount, density = int(random.triangular(0.0,noise_max,0.0)), int(random.triangular(1.0, 100.0, 100.0))
    args.append(['filmgrain', '-a', str(amount), '-A', '0', '-d', str(density), '-b', '0', img, out])
    saveargs.append({'amount': amount, 'density': density})
    
    temperature = int(random.gammavariate(alpha=6, beta=6500/6))
    if temperature < 1000: temperature = 1000
    if temperature > 40000: temperature = 40000
    args.append(['colortemp', '-t', str(temperature), img, out])
    saveargs.append({'temperature': temperature})
    
    exposure = random.normalvariate(mu=0, sigma=30)
    if exposure < -100: exposure = -100
    if exposure > 100: exposure = 100
    args.append(['exposure', '-a', str(exposure), img, out])
    saveargs.append({'exposure': exposure})
    
    defocus = random.triangular(1.0, blur_max, 1.0)
    args.append(['camerablur', '-t', 'defocus', '-a', str(defocus), img, out])
    saveargs.append({'defocus': defocus})
    
    motion = random.triangular(1.0, blur_max, 1.0)
    rotation = random.random() * 360 - 180
    args.append(['camerablur', '-t', 'motion', '-a', str(motion), '-r', str(rotation), img, out])
    saveargs.append({'motion': motion, 'rotation': rotation})
    
    retargs = {}
    for i, argset in enumerate(args):
        r = random.random()
        if (i < 2 and r < 0.3) or i < 0.2:
            subprocess.call(argset)
            retargs.update(saveargs[i])
    
    return os.path.split(img)[-1], retargs
                                             
def images_from_folder(d):
    for r,d,f in os.walk(d):
        for fi in filter(lambda a: os.path.splitext(a)[-1] == '.jpg', f):
            yield os.path.join(r,fi)

def outputs_for_images(indir, outdir, img):
    p = os.path.join(outdir, os.path.relpath(img, start=indir))
    if not os.path.exists(os.path.dirname(p)): os.makedirs(os.path.dirname(p))
    return p

def noise_param_tuple(imgs, indir, outdir, m, uniform=False):
    for img in imgs:
        if uniform:
            yield (img, outputs_for_images(indir, outdir, img), int(random.random()*m))
        else:
            yield (img, outputs_for_images(indir, outdir, img), int(random.triangular(0.0,m,0.0)))
        
def colour_param_tuple(imgs, indir, outdir):
    for img in imgs:
        rand = int(random.gammavariate(alpha=6, beta=6500/6))
        if rand < 1000: rand = 1000
        if rand > 40000: rand = 40000
        yield (img, outputs_for_images(indir, outdir, img), rand)
        
def exposure_param_tuple(imgs, indir, outdir):
    for img in imgs:
        rand = int(random.normalvariate(mu=0, sigma=20))
        if rand < -100: rand = -100
        if rand > 100: rand = 100
        yield (img, outputs_for_images(indir, outdir, img), rand)
        
def blur_param_tuple(imgs, indir, outdir, m=30.0):
    for img in imgs:
        rand = int(random.triangular(1.0, m, 1.0))
        yield (img, outputs_for_images(indir, outdir, img), rand)
        
def all_param_tuple(imgs, indir, outdir, m=30.0):
    for img in imgs:
        yield (img, outputs_for_images(indir, outdir, img))

NOISE_MAX = 500
def process_noise(args):
    imgs = images_from_folder(args.input)
#     outs = outputs_for_images(args.input, args.output, imgs)
#     amounts = noise_level(args.noise_max)
    
#     print [len(list(a)) for a in (imgs, outs)]
    
    params = noise_param_tuple(imgs, args.input, args.output, args.noise_max, uniform=True)#izip(imgs, outs, amounts)
    
    level = Parallel(n_jobs=-2, pre_dispatch='2*n_jobs')(delayed(noise)(img, out, amount) for img,out,amount in params)
    
    dump(level, filename=os.path.join(args.output, 'fileparams.jbl'))

def process_colour(args):
    imgs = images_from_folder(args.input)
    
    params = colour_param_tuple(imgs, args.input, args.output)
    
    level = Parallel(n_jobs=-2)(delayed(colour)(img, out, temp) for img,out,temp in params)
    
    dump(level, filename=os.path.join(args.output, 'fileparams.jbl'))

def process_exposure(args):
    imgs = images_from_folder(args.input)
    params = exposure_param_tuple(imgs, args.input, args.output)
    level = Parallel(n_jobs=-2)(delayed(exposure)(img, out, amount) for img,out,amount in params)
    dump(level, filename=os.path.join(args.output, 'fileparams.jbl'))

def process_focusblur(args):
    imgs = images_from_folder(args.input)
    params = blur_param_tuple(imgs, args.input, args.output)
    level = Parallel(n_jobs=-2)(delayed(focusblur)(img, out, amount) for img,out,amount in params)
    dump(level, filename=os.path.join(args.output, 'fileparams.jbl'))

def process_motionblur(args):
    imgs = images_from_folder(args.input)
    params = blur_param_tuple(imgs, args.input, args.output)
    level = Parallel(n_jobs=-2)(delayed(motionblur)(img, out, amount) for img,out,amount in params)
    dump(level, filename=os.path.join(args.output, 'fileparams.jbl'))

def process_all(args):
    imgs = images_from_folder(args.input)
    assert args.input == args.output, 'For "all", the input and output must be the same.'
    params = all_param_tuple(imgs, args.input, args.output)
    level = Parallel(n_jobs=-2, verbose=2)(delayed(all)(img, out) for img,out in params)
    dump(level, filename=os.path.join(args.output, 'fileparams.jbl'))

if __name__=='__main__':
    '''Example:
    python 
    '''
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', dest='input', help='Folder with the images')
    parser.add_argument('-o', dest='output', help='Where to output the image file')
    
    subparsers = parser.add_subparsers(help='commands')
    
    noise_parser = subparsers.add_parser('noise', help='')
    noise_parser.add_argument('--noise-max', default=NOISE_MAX)
    noise_parser.set_defaults(func=process_noise)
    
    colour_parser = subparsers.add_parser('colour', help='')
    colour_parser.set_defaults(func=process_colour)
    
    exposure_parser = subparsers.add_parser('exposure', help='')
    exposure_parser.set_defaults(func=process_exposure)
    
    exposure_parser = subparsers.add_parser('focus', help='')
    exposure_parser.set_defaults(func=process_focusblur)
    
    exposure_parser = subparsers.add_parser('motion', help='')
    exposure_parser.set_defaults(func=process_motionblur)
    
    exposure_parser = subparsers.add_parser('all', help='')
    exposure_parser.set_defaults(func=process_all)
    
    args = parser.parse_args()
    args.func(args)