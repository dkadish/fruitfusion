from skimage.filter import sobel
from skimage.io import imread, imshow
from skimage.color import rgb2hsv
from scipy import ndimage

import numpy as np

MIN, MAX = 0.25, 0.65
    
def regions(img, min=MIN, max=MAX, visualize=False, output=None):
    '''
    Takes an RGB image as an input.
    '''
    
    # Extract the saturation
#    img = rgb2hsv(img) #FIXME: Should be outside the operator for consistency 
#    hue, sat, val = img[:,:,0], img[:,:,1], img[:,:,2]
    
    # Create the elevation map
    elevation_map = sobel(img)
    
    # Get initial markers
    markers = np.zeros_like(img)
    markers[img < min] = 1
    markers[img > max] = 2
    
    # Perform watershed segmentation 
    from skimage.morphology import watershed
    segmentation = watershed(elevation_map, markers)
    segmentation = ndimage.binary_fill_holes(segmentation - 1)
    labeled_value, _ = ndimage.label(segmentation)
    
    if visualize or output != None:
        import matplotlib
        matplotlib.use('GTK')
        import matplotlib.pyplot as plt
        
        # Grayscale Image
        f_gray = plt.figure()
        plt.imshow(img, cmap='binary')#plt.cm.gray)
#        plt.title('Greyscale Image')
        plt.axis('off')
        
        # Sobel Filtered Image
        f_sobel = plt.figure()
        plt.imshow(elevation_map, cmap='gray')#plt.cm.gray)
#        plt.title('Sobel Filter Result')
        plt.axis('off')
        
        # Markers
        f_markers = plt.figure()
        plt.imshow(markers, cmap='autumn', interpolation='nearest')
#        plt.title('Thresholded Grey Image')
        plt.axis('off')
        
        # Segmented Regions
        f_segmented = plt.figure()
        plt.imshow(img, cmap='binary', interpolation='nearest')
        plt.contour(segmentation, [0.5], linewidths=1.2, colors='r')
#        plt.title('Segmentation Overlay on Grey Image')
        plt.axis('off')
        
        # Labeled regions
        f_regions = plt.figure()
        plt.imshow(labeled_value, cmap='autumn', interpolation='nearest')
#        plt.title('Image Segmentation')
        plt.axis('off')
        
#        if visualize: plt.show()
        if output != None:
            from matplotlib.backends.backend_pdf import PdfPages
            pp = PdfPages(output)
            f_gray.savefig(pp, format='pdf')
            f_sobel.savefig(pp, format='pdf')
            f_markers.savefig(pp, format='pdf')
            f_segmented.savefig(pp, format='pdf')
            f_regions.savefig(pp, format='pdf')
            pp.close()
            
        if visualize:
            plt.show()
    
    return segmentation, labeled_value

def _main_regions(args):
    img = imread(args.img)
    img = rgb2hsv(img) #FIXME: Should be outside the operator for consistency 
    hue, sat, val = img[:,:,0], img[:,:,1], img[:,:,2]
    min = args.min != None and args.min or MIN
    max = args.max != None and args.max or MAX
    regions(sat, min, max, visualize=True, output=args.output)

if __name__=='__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-i', dest='img', help='Input image file')
    parser.add_argument('-o', dest='output', help='Where to output the image file')
    parser.add_argument('--min', dest='min', help='MIN', default=None)
    parser.add_argument('--max', dest='max', help='MAX', default=None)
    
    subparsers = parser.add_subparsers(help='commands')
    regions_parser = subparsers.add_parser('regions', help='Performs region-based segmentation...')
    regions_parser.set_defaults(func=_main_regions)
    
    args = parser.parse_args()
    args.func(args)