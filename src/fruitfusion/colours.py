import sys, os
from skimage.transform.integral import integral_image
from scipy.stats.kde import gaussian_kde
sys.path.append(os.path.abspath(os.path.join(__file__,'../../')))

from skimage.color import rgb2hsv
from skimage.exposure._adapthist import equalize_adapthist
from skimage.color.colorconv import rgb2xyz
from tools import plot_img_and_hist, plot_img
from skimage.exposure import exposure
from background import regions
from skimage.io import imread
import numpy as np

def wnukowicz(rgb, thresh=0.05, f=3):
    xyz = rgb2xyz(rgb)
    x,y,z = [xyz[:,:,i] for i in xrange(3)]
    
    ymin, ymax = np.min(y), np.max(y)
    T_lum = ymax-(ymax-ymin)*thresh # Threshild
    
    # Identify the light and dark pixels
    p = np.ones(shape=y.shape)
    p[y<T_lum] = 0
    
    # Create necessary structures
    integ_img = integral_image(x)
    rowcol = np.float64(np.repeat(np.arange(1,x.shape[0]+1)[:,np.newaxis], x.shape[1], axis=1) * np.repeat(np.arange(1,x.shape[1]+1)[:,np.newaxis], x.shape[0], axis=1).T)
    
    X_a_full = (1.0/rowcol) * integ_img
    
    X_a = np.average(X_a_full[p != 0])
    X_Ts = [f*X_a,0]
    
    while X_Ts[0] != X_Ts[-1]:
        p = np.ones(p.shape)
        p[x > X_Ts[0]] = 0
        
        X_a = np.average(X_a_full[p != 0])
        X_Ts[-1] = X_Ts[0]
        X_Ts[0] = f*X_a
    
    Y_a = np.average(((1.0/rowcol) * integral_image(y))[p!=0])
    Z_a = np.average(((1.0/rowcol) * integral_image(z))[p!=0])
    
    x_s = X_a / (X_a + Y_a + Z_a)
    y_s = Y_a / (X_a + Y_a + Z_a)
    
    return x_s / y_s

BRIGHT_FRACT = 0.05
def temperature(hue, sat, val, bright_fract=BRIGHT_FRACT):
    '''    
    '''
    # Find the brightest pixels
    satval = (1-sat)*val
    min, max = np.min(satval), np.max(satval)
    r = max - min
    maxrange = max - bright_fract*r
    
    bright_pix = np.logical_and(satval > maxrange, satval <= max)
    
#     print 'NUMBER OF PIXELS: ', np.sum(bright_pix), maxrange, max
        
    # Take the average hue
    av_bright_hue = np.average(hue[bright_pix])
    
#     print 'SAT: ', np.average(sat[bright_pix])
    
    # Return it as the temperature estimate
    return av_bright_hue

def white_balance(rgb):
    eq = equalize_adapthist(rgb.copy())
    hsv = rgb2hsv(rgb)
    hsv_eq = rgb2hsv(eq)
    
    h = hsv[:,:,0] - hsv_eq[:,:,0]
    
    return np.average(h)

def perfect_reflector(rgb):
    '''Based on the perfect reflector method...
    http://scien.stanford.edu/pages/labsite/2000/psych221/projects/00/trek/PRimages.html
    '''
    rbg = np.float64(rgb)
    xyz = rgb2xyz(rgb)
    ymax = np.max(xyz[:,:,1]) # Brightest pixels
    
    mask = xyz[:,:,1] == ymax # Locations of the brightest pixels
    
    rgb_max = rgb[mask[:,:],:] # An array of the rgb values of the brightest pixels
    
    avg = np.average(rgb_max,axis=0) # Average values of the brightest pixels
    avg[avg==0.0] = 1.0 #FIXME: hack?
    
    rgb_scaled = rgb * (255.0/np.average(rgb_max,axis=0)) # Scale so that the brightest pixels are at (255,255,255)
    
    maxval = np.max(rgb_scaled)
    if maxval > 255.0:
        rgb_scaled /= maxval

    rgb_avg = np.average(np.average(rgb - rgb_scaled, axis=0),axis=0) # Difference between the white-balanced and original image
    
    #TODO: could it help to return the average hue difference between the images?
    return rgb_avg
    
def contrast(img, visualize=False, output=None):
    '''Is there a better definition of contrast?
    Histogram variance?
    '''
    eq = exposure.equalize_hist(img)
    diff = np.abs(eq - img)
    
    if visualize: plot_img(diff)
    
    return np.average(diff)

def level(img, visualize=False, output=None):
#    if visualize: plot_img(diff)
    
    return np.average(img)

def histogram_variance(img, mask=None, visualize=False, output=None):
    '''Takes the histogram of an image and returns the variance as a measure
    of contrast.
    
    img could be hue, sat, var, L, a, or b, depending on what measure we are
    looking for.
    '''
    hist, bin_edges = histogram(img, mask, visualize)
    variance = np.var(hist)
    
    if visualize: print 'Variance: ', variance
    
    return variance

def histogram_entropy(img, mask=None, visualize=False, output=None):
    '''Takes the histogram of an image and returns the entropy as a measure
    of contrast.
    
    img could be hue, sat, var, L, a, or b, depending on what measure we are
    looking for.
    '''
    hist, bin_edges = histogram(img, mask, visualize)
    s = hist[hist!=0] * np.log2(hist[hist!=0])
    entropy = -np.sum(hist[hist!=0] * np.log2(hist[hist!=0]))
    
    if visualize: print 'Entropy: ', entropy
    
    return entropy

def histogram(img, mask=None, normalize=True, visualize=False, output=None):
    if mask == None:
        hist, bin_edges = np.histogram(img, bins=np.arange(0.0,1.0,1.0/257.0))
        
        if normalize:
            hist = np.float64(hist)*256.0/img.size
    else:
        hist, bin_edges = np.histogram(img[mask != 0], bins=np.arange(0.0,1.0,1.0/257.0))
        img[mask == 0] = -1
        
        if normalize:
            hist = np.float64(hist)*256.0/img[mask!=0].size

    if visualize or output!=None: plot_img_and_hist(img, bin_edges[:-1], hist, xlabel='Pixel value', ylabel='Pixel count', colour='spectral', visualize=visualize, output=output)
    
    
    return hist[:,np.newaxis], bin_edges
    
def _main_contrast(args):
    img = imread(args.img)
    hsv = rgb2hsv(img)
    hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
    cont = contrast(val, visualize=True)
    
    print cont
    
def _main_histogram(args):
    img = imread(args.img)
    img += np.random.normal(loc=0.0, scale=0.05*(np.max(img)-np.min(img)), size=img.shape)
    hsv = rgb2hsv(img)
    hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
    segmentation, labeled_value = regions(sat)
    hist = histogram(hue, segmentation, visualize=True, output=args.output) 
    
def _main_histogram_entropy(args):
    img = imread(args.img)
    hsv = rgb2hsv(img)
    segmentation=None
    hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
    hist = histogram_entropy(hue, segmentation, visualize=True)
    
def _main_histogram_variance(args):
    img = imread(args.img)
    hsv = rgb2hsv(img)
    hue, sat, val = hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]
    segmentation=None
    hist = histogram_variance(hue, segmentation, visualize=True)  

if __name__=='__main__':
    '''Example:
    python colours.py -i ~/documents/school/grad/projects/ai/tropical-fruits/agata_potato/agata_potato_016.jpg hist
    '''
    import argparse
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-i', dest='img', help='Input image file')
    parser.add_argument('-o', dest='output', help='Where to output the image file')
    
    subparsers = parser.add_subparsers(help='commands')
    histogram_parser = subparsers.add_parser('hist', help='Extracts histograms from images.')
    histogram_parser.set_defaults(func=_main_histogram)
    
    contrast_parser = subparsers.add_parser('contrast', help='Extracts contrasts from images.')
    contrast_parser.set_defaults(func=_main_contrast)
    
    contrast_parser = subparsers.add_parser('hist_ent', help='Extracts histogram entropies from images.')
    contrast_parser.set_defaults(func=_main_histogram_entropy)
    
    contrast_parser = subparsers.add_parser('hist_var', help='Extracts histogram variance from images.')
    contrast_parser.set_defaults(func=_main_histogram_variance)
    
    args = parser.parse_args()
    args.func(args)