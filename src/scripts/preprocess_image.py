#!/usr/local/bin/python2.7
# encoding: utf-8
'''
scripts.add_noise -- shortdesc

scripts.add_noise is a description

It defines classes_and_methods

@author:     user_name
        
@copyright:  2012 organization_name. All rights reserved.
        
@license:    license

@contact:    user_email
@deffield    updated: Updated
'''

import sys
import os

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from skimage.io import imread, imsave
import random
import numpy as np
from scipy.ndimage.filters import gaussian_filter
from sklearn.externals import joblib

__all__ = []
__version__ = 0.1
__date__ = '2012-11-27'
__updated__ = '2012-11-27'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

def noise(img, level=None):
    if level == None:
        level = random.random()*0.1*255
    else:
        level = level*0.1*255
    img += np.random.normal(loc=0.0, scale=level, size=img.shape)
    return img

def blur(img, level=None):
    if level == None:
        level = random.random()
    return gaussian_filter(img, sigma=level)

def darken(img, level=None):
    if level == None:
        level = random.random()
    return img/(level+1)

def lighten(img, level=None):
    if level == None:
        level = random.random()
    return 255 - (255-img)/(level+1)

def fix_ranges(img):
    img[img<0] = 0
    img[img>255] = 255
    return np.uint8(img)

def process(f):
    img = np.int16(imread(f))
    
    r = random.random()
    if r < 0.2:
        # Apply Noise
        img = noise(img)
    elif r < 0.4:
        # Apply Gaussian blur
        img = blur(img)
        
    r = random.random()
    if r < 0.2:
        img = darken(img)
    elif r < 0.4:
        img = lighten(img)
    
    img = fix_ranges(img)
    
    return img

def load(f, images, dest):
    try:
        img = process(images[f])
    except IOError:
        print 'Could not process image %s' %images[f]
    dir = os.path.split(os.path.join(dest,f))[0]
    if not os.path.exists(dir):
        os.makedirs(dir)
    imsave(os.path.join(dest,f), img)
        
def preprocess(src, dest):
    # Make a list of images
    images = {}
    for r,d,f in os.walk(src):
        for fi in filter(lambda a: os.path.splitext(a)[-1] == '.jpg', f):
            images[os.path.relpath(os.path.join(r,fi), src)] = os.path.join(r,fi)
    
    joblib.Parallel(n_jobs=6, pre_dispatch='2*n_jobs', verbose=1)(joblib.delayed(load)(i, images, dest) for i in images)
#    for i in images:
#        load(i, images, dest)
    
def main(argv=None): # IGNORE:C0111
    '''Command line options.'''
    
    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
#    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by user_name on %s.
  Copyright 2012 organization_name. All rights reserved.
  
  Licensed under the Apache License 2.0
  http://www.apache.org/licenses/LICENSE-2.0
  
  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' #% (program_shortdesc, str(__date__))

#    try:
    # Setup argument parser
    parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument("-r", "--recursive", dest="recurse", action="store_true", help="recurse into subfolders [default: %(default)s]")
    parser.add_argument("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %(default)s]")
    parser.add_argument('-V', '--version', action='version', version=program_version_message)
    parser.add_argument("input", help="path to the folder of clean images", metavar="IN")
    parser.add_argument("output", help="path to the output folder", metavar="OUT")
    parser.add_argument("-d", "--demo", dest="demo", action="store_true", help="run as demo [default: %(default)s]", default=False)
    # Process arguments
    args = parser.parse_args()
    
    verbose = args.verbose
    recurse = args.recurse
    input = args.input
    output = args.output
    
    if verbose > 0:
        print("Verbose mode on")
        if recurse:
            print("Recursive mode on")
        else:
            print("Recursive mode off")
    
    if args.demo:
        img = np.int16(imread(input))
        
        noisy = fix_ranges(noise(img.copy(), level=2.5))
        blurry = fix_ranges(blur(img.copy(), level=0.75))
        
        dark = fix_ranges(darken(img.copy(), level=0.5))
        light = fix_ranges(lighten(img.copy(), level=0.5))
        
        imsave(os.path.join(output,'clean.png'), fix_ranges(img))
        imsave(os.path.join(output,'noisy.png'), noisy)
        imsave(os.path.join(output,'blurry.png'), blurry)
        imsave(os.path.join(output,'dark.png'), dark)
        imsave(os.path.join(output,'light.png'), light)
    else:
        preprocess(input, output)
    
    return 0
#    except KeyboardInterrupt:
#        ### handle keyboard interrupt ###
#        return 0
#    except Exception, e:
#        if DEBUG or TESTRUN:
#            raise(e)
#        indent = len(program_name) * " "
#        sys.stderr.write(program_name + ": " + repr(e) + "\n")
#        sys.stderr.write(indent + "  for help use --help")
#        return 2

if __name__ == "__main__":
    if DEBUG:
#        sys.argv.append("-h")
        sys.argv.append("-v")
        sys.argv.append("-r")
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'scripts.add_noise_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())