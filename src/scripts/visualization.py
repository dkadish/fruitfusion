import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics.metrics import f1_score
from matplotlib import cm
from scripts import get_rank_of_truth, N_FRUITS
from sklearn.feature_selection.rfe import RFE
from sklearn.ensemble.forest import ExtraTreesClassifier
from sklearn.feature_selection import chi2
from sklearn.feature_selection.univariate_selection import f_classif
from matplotlib.ticker import AutoMinorLocator
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from fruitfusion.context.multiclass.locator import correctness
from fruitfusion.context.multiclass import dict_to_array
import itertools

map_colour = lambda r,g,b,name:  LinearSegmentedColormap(name=name, segmentdata={
    'red':[(0.0,1.0,1.0),(1.0,r/255.0,r/255.0)],
    'green':[(0.0,1.0,1.0),(1.0,g/255.0,g/255.0)],
    'blue':[(0.0,1.0,1.0),(1.0,b/255.0,b/255.0)],
})

Yellows = LinearSegmentedColormap(name='Yellows', segmentdata={
    'red':[(0.0,1.0,1.0),(1.0,1.0,1.0)],
    'green':[(0.0,1.0,1.0),(1.0,1.0,1.0)],
    'blue':[(0.0,1.0,1.0),(1.0,0.2,0.2)],
})

Turquoises = map_colour(72,209,204,'Turquoises')

VioletReds = map_colour(199,21,133,'VioletReds')

SEQUENTIAL_CM = [cm.Reds, cm.Blues, cm.Greens, cm.Purples, cm.Oranges, Yellows]
VOTE_CM = [cm.Reds, cm.Blues, cm.Greens, cm.Purples, VioletReds, Turquoises]
darkcmap_vote = ListedColormap(colors=[c(0.75) for c in SEQUENTIAL_CM[:4]] + ['MediumTurquoise'] + [c(0.75) for c in SEQUENTIAL_CM[4:]], name='', N=len(SEQUENTIAL_CM)+1)
darkcmap_contours = ListedColormap(colors=[c(0.75) for c in VOTE_CM], name='', N=len(VOTE_CM))
darkcmap = ListedColormap(colors=[c(0.75) for c in SEQUENTIAL_CM], name='', N=len(SEQUENTIAL_CM))
lightcmap = ListedColormap(colors=[c(0.5) for c in SEQUENTIAL_CM], name='', N=len(SEQUENTIAL_CM))
pairedcmap_vote = ListedColormap(colors=itertools.chain(*[(c(0.25),c(0.75)) for c in VOTE_CM]), name='', N=len(VOTE_CM)*2)
pairedcmap = ListedColormap(colors=itertools.chain(*[(c(0.25),c(0.75)) for c in SEQUENTIAL_CM]), name='', N=len(SEQUENTIAL_CM)*2)

PDF = 'plots/cdfna-orig/visualize-%s.pdf'
if PDF != None:
    from matplotlib.backends.backend_pdf import PdfPages

def _open_plot(n=''):
    pp = None
    if PDF != None:
        pp = PdfPages(PDF %(n))
    return pp

def _close_plot(pp=None):
    if PDF == None:
        plt.show()
    else:
        pp.savefig()
        pp.close()

def plot_compared_scores(cdf_i, cdf_e, X_test_i, X_test_e, y_test, names, wv=None):
    '''Plots the F1 scores of all of the classifiers and the fused classifier against each other.
    '''
    pp =_open_plot('compared')
        
    colours = wv == None and darkcmap or darkcmap_vote
    l = wv == None and 2 or 3
        
    f1 = np.zeros((len(cdf_e.primary) + l, 1))
    for i, p in enumerate(cdf_e.primary):
        y_true, y_pred = y_test[p], cdf_e.primary[p].predict(X_test_e[p])
        f1[i,-1] = f1_score(y_true, y_pred)
    
    if wv != None:
        y_true, y_pred = y_test[0], wv.predict(X_test_i)
        f1[-3,-1] = f1_score(y_true, y_pred)
    
    y_true, y_pred = y_test[0], cdf_i.predict(X_test_i)
    f1[-2,-1] = f1_score(y_true, y_pred)
    y_true, y_pred = y_test[0], cdf_e.predict(X_test_e)
    f1[-1,-1] = f1_score(y_true, y_pred)

    
    fig, ax = bar_chart(f1, names, labels=[''], x_label='', y_label='F1 Score', title='', space=4, colours=colours)
    
    ax.set_ylim(top=1)
    
    _close_plot(pp)

def plot_compared_base_scores(cdf, X_test, y_test):
    '''Plots the F1 scores of all of the classifiers and the fused classifier against each other.
    '''
    if PDF != None:
        pp = PdfPages(PDF %('base-compared'))
        
    names = ('simple vote', 'context-dependent fusion')
        
    primary = cdf.primary.predict(X_test)
    primary = np.array([primary[p] for p in primary]).T
    
    vote_count = []
    for i in range(len(cdf.classes)):
        pos = np.int16(primary == i) # Where the votes are for the class
        
        vote_count.append(np.sum(pos, axis=1))
    vote_count = np.array(vote_count).T
    
    base_predict = np.argmax(vote_count, axis=1)    
        
    labels = range(N_FRUITS)
    labels.append('avg')
    
    f1 = np.zeros((2, len(labels)))
    f1[0,:-1] = f1_score(y_test[p], base_predict, average=None)
    f1[0,-1] = f1_score(y_test[p], base_predict)

    y_true, y_pred = y_test[0], cdf.predict(X_test)
    f1[-1,:-1] = f1_score(y_true, y_pred, average=None)
    f1[-1,-1] = f1_score(y_true, y_pred)
    
    fig, ax = bar_chart(f1, names, labels, x_label='class', y_label='percentage', title='F1 scores, compared to base case')
    
    ax.set_ylim(top=1)
    
    if PDF == None:
        plt.show()
    else:
        pp.savefig()
        pp.close()
        
def plot_compared_all(cdf, X_test, y_test, names):
    if PDF != None:
        pp = PdfPages(PDF %('all-compared'))
        
    labels = range(N_FRUITS)
    labels.append('avg')
    
    # Primary Classifiers
    f1 = np.zeros((len(cdf.primary) + 2, 1))#, len(labels)))
    for i, p in enumerate(cdf.primary):
        y_true, y_pred = y_test[p], cdf.primary[p].predict(X_test[p])
        f1[i,-1] = f1_score(y_true, y_pred)

    # Simple Vote   
    primary = cdf.primary.predict(X_test)
    primary = np.array([primary[p] for p in primary]).T
    
    vote_count = []
    for i in range(len(cdf.classes)):
        pos = np.int16(primary == i) # Where the votes are for the class
        
        vote_count.append(np.sum(pos, axis=1))
    vote_count = np.array(vote_count).T
    
    base_predict = np.argmax(vote_count, axis=1)  
    f1[-2,-1] = f1_score(y_true, base_predict)
    
    # CDF
    y_true, y_pred = y_test[0], cdf.predict(X_test)
    f1[-1,-1] = f1_score(y_true, y_pred)
    
    # Plot
    data = f1
#    x_label='classifier'
    y_label='F1 score'
    title='F1 scores, by classifier'
    
    rects = []
    ind = np.arange(data.shape[1])
    space_between = 0.3
    width = (1.0-space_between)/(data.shape[0]+2)
    
    # Create the plot
    fig = plt.figure()
    ax = fig.add_subplot(111)
#    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_title(title)
    ax.set_xticks([])
    ml = AutoMinorLocator()
    ax.yaxis.set_minor_locator(ml)
    ax.grid(which='major', axis='y', color='0.1', linestyle='-', linewidth=0.5)
    ax.grid(which='minor', axis='y', color='0.2', linestyle='-', linewidth=0.25)
    ax.set_axisbelow(True)
#    ax.set_xticklabels( labels )
    
    for i in range(data.shape[0]-2):
        rects.append( ax.bar(ind+i*width, data[i,:], width, color=cm.Paired(i/12.0)) ) # http://colorbrewer2.org/, alternatively, Set3
        
    for i in range(data.shape[0]-2, data.shape[0]):
        rects.append( ax.bar(ind+i*width+width*0.5, data[i,:], width, color=cm.Paired(i/12.0)) ) # http://colorbrewer2.org/, alternatively, Set3
    
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
    ax.legend( (r[0] for r in rects), names, ncol=3, loc='upper center', bbox_to_anchor=(0.5,-0.1), fontsize='small')
    
    ax.set_ylim(top=1)
    
    if PDF == None:
        plt.show()
    else:
        pp.savefig()
        pp.close()
    
def plot_rankings(cdf, X_test, y_test, names):
    '''Plots the ranking of the correct class over the testing set for all classifiers.
    '''
    labels = range(N_FRUITS)
    labels.append('avg')
    
    primary_rankings = []
    for p in cdf.primary:
        primary_rankings.append( get_rank_of_truth(cdf.primary_rankings(X_test)[p], y_test[p][:,np.newaxis]) )
    
    truth = y_test[0][:,np.newaxis]
    probs = cdf.predict_proba(X_test)
    order = np.argsort(probs)
    ranking = (order.shape[1] * np.ones(truth.shape)) - np.argwhere(order == truth)[:,1:] - 1
    
    primary_rankings.append(ranking)
    
    hist = np.zeros((len(primary_rankings), N_FRUITS))
    for i, p in enumerate(primary_rankings):
        hist[i,:] = np.histogram(p, bins=range(N_FRUITS+1))[0]
        
    fig, ax = bar_chart(hist, names, labels, x_label='ranking', y_label='count', title='Ranking of the correct class, by classifier')
    
    plt.show()
 
def plot_n_selections(dcs_i, dcs_e, X_test_i, X_test_e, y_test, names):
    '''Plot the number of times a given classifier is selected in a DCS scheme.
    '''
    pp = _open_plot('n-selections')
    
    y_test = y_test[0]
    
    y_pred = dcs_e.predict(X_test_e)
    selected = dcs_e.fuser.predict_selected
    times_selected = np.bincount(selected)
    data_e = times_selected[:,np.newaxis]
    
    y_pred = dcs_i.predict(X_test_i)
    selected = dcs_i.fuser.predict_selected
    times_selected = np.bincount(selected)
    data_i = times_selected[:,np.newaxis]
    
    data = np.concatenate([data_i,data_e],axis=1).ravel()[:,np.newaxis]
    
    bar_chart(data, names, labels=[''], x_label='Primary classifier', y_label='Number of samples', title='', colours=pairedcmap)
    
    _close_plot(pp)

def plot_selected_performance(dcs_i, dcs_e, X_test_i, X_test_e, y_test, names):
    '''Plot the performance of selected classifiers in a DCS scheme
    
    Horizontal size indicates the relative number of selections.
    '''
    pp = _open_plot('selected-performance')
    
    y_test = y_test[0]
    
    # External
    y_pred = dcs_e.predict(X_test_e)
    selected = dcs_e.fuser.predict_selected
    times_selected = np.bincount(selected)
    correct_count = np.bincount(selected[y_pred == y_test])
    
    data_e = np.float64(correct_count)/np.float64(times_selected)
    data_e = data_e[:,np.newaxis]
    
    # Internal
    y_pred = dcs_i.predict(X_test_i)
    selected = dcs_i.fuser.predict_selected
    times_selected = np.bincount(selected)
    correct_count = np.bincount(selected[y_pred == y_test])
    
    data_i = np.float64(correct_count)/np.float64(times_selected)
    data_i = data_i[:,np.newaxis]
    
    data = np.concatenate([data_i,data_e],axis=1).ravel()[:,np.newaxis]
    
    fig, ax = bar_chart(data, names, labels=[''], x_label='Primary classifier', y_label='Accuracy', title='', colours=pairedcmap)
    
    ax.set_ylim(top=1)
    
    _close_plot(pp)

def plot_n_correct_classifiers(cdf, X_test, y_test):
    '''Plot the number of samples with N correct classifiers.
    '''
    pp = _open_plot('correct-classifiers')
    
    y_test = y_test[0]
    
    correct = dict_to_array(correctness(X_test, y_test, cdf.primary))
    n_correct = np.int64(np.sum(correct, axis=1))
    
    proportion = np.float64(np.bincount(n_correct))[np.newaxis,:]
    
    fig, ax = bar_chart(proportion, names=[''], labels=np.arange(0,proportion.size,1), x_label='Number of correct primary classifiers', y_label='Number of samples', title='')
    ax.legend_ = None
    
    _close_plot(pp)

def plot_fusion_type(cdf_i, cdf_e, X_test_i, X_test_e, y_test, names):
    '''Plot the number of times that a classifier was selected in a KNeighboursSelectOrVote fusion scheme.
    '''
    pp = _open_plot('fusion-mechanism')
    y_test = y_test[0]
    X = [X_test_i, X_test_e]
    
    ind_count, fusion = [],[]
    data = np.zeros(shape=(len(names)/2, 2))
    performance = np.zeros(shape=(len(names)/2, 2))
    for i,cdf in enumerate([cdf_i, cdf_e]):
        cdf_perf = cdf.predict(X[i]) == y_test
        
        individuals, ind_mask = cdf.fuser.individuals
        ind_count.append(np.bincount(individuals[ind_mask]))
        fusion.append(cdf.fuser.fusion)
        
        data[:ind_count[-1].size,i] = ind_count[-1]
        data[-2:,i] = fusion[-1][-2:]
        
        for j in range(ind_count[-1].size):
            performance[j, i] = np.average(cdf_perf[np.logical_and(individuals == j, ind_mask)])
            
        performance[-2,i] = np.average(cdf_perf[cdf.fuser.fusion_masks['rvote']])
        performance[-1,i] = np.average(cdf_perf[cdf.fuser.fusion_masks['wvote']])
        
    data = data.ravel()[:,np.newaxis]
    
    bar_chart(data, names, labels=[''], x_label='Mechanism', y_label='Number of samples', title='', ncol=6, colours=pairedcmap_vote)
    
    if pp != None: pp.savefig()
    
    plt.figure()
    
    performance = performance.ravel()[:,np.newaxis]
    
    bar_chart(performance, names, labels=[''], x_label='Mechanism', y_label='F1 Score', title='', ncol=6, colours=pairedcmap_vote)
    
    
#     # Custom Bar Chart
#     rects = []
#     ind = np.arange(data.shape[1])
#     space_between = 0.3
#     width = (1.0-space_between)
#     
#     # Create the plot
#     fig = plt.figure()
#     ax = fig.add_subplot(111)
#     ax.set_xlabel('Fusion Type')
#     ax.set_ylabel('Count')
# #     ax.set_title(title)
#     ax.set_xticks([])#ind+(1.0-space_between)/2)
# #    ax.set_xticklabels( labels )
#     
#     for i in range(data.shape[0]):
#         rects.append( ax.bar(ind[0], data[i,0], width, bottom=np.sum(data[:i,0], axis=0), color=[darkcmap(i)]) ) # http://colorbrewer2.org/, alternatively, Set3
#         
#     for i in range(1,data.shape[1]):
#         c = ['MediumVioletRed','MediumTurquoise']
#         rects.append( ax.bar(ind[i], data[0,i], width, color=c[i-1]) ) # http://colorbrewer2.org/, alternatively, Set3
#     
#     names = (i for i,r in enumerate(rects))
#     
#     box = ax.get_position()
#     ax.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
#     ax.legend( (r[0] for r in rects), names, ncol=4, loc='upper center', bbox_to_anchor=(0.5,-0.1), fontsize='small')
    
    _close_plot(pp)
    
def plot_fused_performance(dcs_i, dcs_e, X_test_i, X_test_e, y_test, names):
    '''Plot the performance of selected classifiers in a DCS scheme
    
    Horizontal size indicates the relative number of selections.
    '''
    pp = _open_plot('selected-performance')
    
    y_test = y_test[0]
    
    # External
    y_pred = dcs_e.predict(X_test_e)
    selected = dcs_e.fuser.predict_selected
    times_selected = np.bincount(selected)
    correct_count = np.bincount(selected[y_pred == y_test])
    
    data_e = np.float64(correct_count)/np.float64(times_selected)
    data_e = data_e[:,np.newaxis]
    
    # Internal
    y_pred = dcs_i.predict(X_test_i)
    selected = dcs_i.fuser.predict_selected
    times_selected = np.bincount(selected)
    correct_count = np.bincount(selected[y_pred == y_test])
    
    data_i = np.float64(correct_count)/np.float64(times_selected)
    data_i = data_i[:,np.newaxis]
    
    data = np.concatenate([data_i,data_e],axis=1).ravel()[:,np.newaxis]
    
    fig, ax = bar_chart(data, names, labels=[''], x_label='Primary classifier', y_label='Accuracy', title='', colours=pairedcmap)
    
    ax.set_ylim(top=1)
    
    _close_plot(pp)

def map_zones_ranked(cdf, X_test, y_test, names, ctx_features=[2,6]):
    X_ctx = cdf.context_normalizer.transform(X_test['context'])
    xx, yy, zz = create_mesh_grid(X_ctx, ctx_features=ctx_features)
            
    Z = []
    for i, p in enumerate(cdf.primary):
        Z.append(find_mesh_results(cdf.context[p], xx, yy, zz, ctx_features))
        
        ranking = get_rank_of_truth(cdf.primary_rankings(X_test)[p], y_test[p][:,np.newaxis])
        
        fig, ax = space_map(X_ctx, ranking, xx, yy, Z[i], x_label='', y_label='', title=names[i], ctx_features=ctx_features)
        
        plt.show()
    
    truth = y_test[0][:,np.newaxis]
    probs = cdf.predict_proba(X_test)
    order = np.argsort(probs)
    ranking = (order.shape[1] * np.ones(truth.shape)) - np.argwhere(order == truth)[:,1:] - 1
    
    fig, ax = overlayed_space_map(X_ctx, ranking, xx, yy, Z, x_label='', y_label='', title='CDF', ctx_features=ctx_features)
    
    plt.show()
    
def map_zones_binary(cdf, X_test, y_test, names, ctx_features=[2,6]):
    if PDF != None:
        pp = PdfPages(PDF %('locales'))
    
    X_ctx = X_test['context']
    X_norm = cdf.context_normalizer.transform(X_ctx)
#    print np.min(X_norm), np.max(X_norm), np.min(X_ctx), np.max(X_ctx)
        
    prediction = cdf.predict(X_test)
#    print 'Y_test: ', y_test[0][:15]
    correct = np.zeros(y_test.values()[0].shape)
    correct[prediction==y_test.values()[0]] = 1
    
    nonzero = np.all(X_norm>0, axis=1)
#    print np.sum(nonzero), X_norm.shape, correct.shape, np.min(X_norm), np.max(X_norm)
    chi, pval = chi2(X_norm[nonzero,:], correct[nonzero])
    features = np.argsort(chi)
    print features, np.argsort(pval)
    ctx_features = [2,3]#features[-6:-4]
    print ctx_features
    
    xx, yy, zz = create_mesh_grid(X_norm, ctx_features=ctx_features)
            
    Z = []
    
    correct_p = cdf.primary_truth(cdf.primary.predict(X_test), y_test.values()[0])
    
    for i, p in enumerate(cdf.primary):
        # Try to figure out the most important features for the correctness/incorrectness of the classifier
        
#        print np.all(ctx>0, axis=1).shape, correct[p].shape
        nonzero = np.all(X_norm>0, axis=1)
#        print np.sum(nonzero), X_norm.shape, correct_p[p].shape, np.min(X_norm), np.max(X_norm)
        chi, pval = chi2(X_norm[nonzero,:], correct_p[p][nonzero])
#        chi, pval = f_classif(X_norm, correct_p[p])
#        print chi, pval
        features = np.argsort(chi)
        print features, np.argsort(pval)
        ctx_features_p = [2,3]#features[-6:-4]
        print ctx_features_p
        
        xx_p, yy_p, zz_p = create_mesh_grid(X_norm, ctx_features=ctx_features_p)
        z = np.float64(find_mesh_results(cdf.context[p], xx, yy, zz, ctx_features))
        
        Z_p = np.float64(find_mesh_results(cdf.context[p], xx_p, yy_p, zz_p, ctx_features_p))
        
#        print cdf.local_weights[p].dtype, cdf.local_weights[p], cdf.local_weights[p].size
        for j in range(cdf.local_weights[p].size):
#            print cdf.local_weights[p][j], np.sum(Z_p == j), np.unique(Z_p)
            Z_p[Z_p == j] = cdf.local_weights[p][j]
            z[z==j] = cdf.local_weights[p][j]
            
        Z.append(z)
        
        fig, ax = space_map(X_norm, correct_p[p], xx_p, yy_p, Z_p, x_label='', y_label='', title=names[i], ctx_features=ctx_features_p)
        
        if PDF == None:
            plt.show()
        else:
            pp.savefig()
    
    fig, ax = overlayed_space_map(X_norm, correct, xx, yy, Z, x_label='', y_label='', title='CDF', ctx_features=ctx_features)
    
    if PDF == None:
        plt.show()
    else:
        pp.savefig()
        pp.close()
        
def map_cdf_selected_zones(cdf, X_test, y_test, names, ctx_features, t='', plot_primary=True, cm=darkcmap):
    pp = _open_plot('cdf-zones%s' %(t != '' and '-%s' %t))
    
    X_ctx = X_test['context']
    X_norm = cdf.normalizer.transform(X_ctx)
    y_pred = cdf.predict(X_test)
    correct = y_pred == y_test[0]
    incorrect = np.logical_not(correct)
    
    X1 = X_norm[:,ctx_features]
    
    # Plot the space
    h = 0.02
    x_min, x_max = X1[:, 0].min() - 0.25, X1[:, 0].max() + 0.25
    y_min, y_max = X1[:, 1].min() - 0.25, X1[:, 1].max() + 0.25
    # Generate lists for the other axes
    meds = np.median(X_norm, axis=0)
    
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                     np.arange(y_min, y_max, h))
    xy_raveled = np.c_[xx.ravel(), yy.ravel()]
    
    grid = []
    for i,m in enumerate(meds):
        if i == ctx_features[0]:
            grid.append(np.arange(x_min, x_max, h))
        elif i == ctx_features[1]:
            grid.append(np.arange(y_min, y_max, h))
        else:
            grid.append(np.ones(shape=np.arange(x_min, x_max, h).shape) * m)
            
    raveled = np.ones(shape=(xy_raveled.shape[0], meds.shape[0])) * meds
    raveled[:, ctx_features[0]] = xy_raveled[:,0]
    raveled[:, ctx_features[1]] = xy_raveled[:,1]
    
    locale = cdf.segmenter.predict(raveled)
    predict = dict([(p, np.random.randint(0,14,size=(locale.shape[0],))) for p in cdf.primary])
    cdf.fuser.predict(predict, locale)
    Z = cdf.fuser.predict_selected
    Z = Z.reshape(xx.shape)
    
    if plot_primary:
        # Plot the scores for each classifier in each area
        performance = cdf.scores(raveled)
        pmin, pmax = np.min(performance), np.max(performance)
        plt.figure()
        for i,p in enumerate(cdf.primary):
            plt.subplot((len(cdf.primary)+1) / 2, 2, i+1) # Create the subplot
            score = performance[:,i]
            S = score.reshape(xx.shape)
            plt.title(names[i])
            print 'Min/Max: ', np.min(score), np.max(score)
            CS = plt.contourf(xx, yy, S, cmap=SEQUENTIAL_CM[i % 4], levels=np.linspace(pmin, pmax, 20))
            plt.scatter(X1[correct,0], X1[correct,1],c='g',marker='o',alpha=0.5)#, s, c, marker, cmap, norm, vmin, vmax, alpha, linewidths, faceted, verts, hold)
            plt.scatter(X1[incorrect,0], X1[incorrect,1],c='r',marker='o',alpha=0.5)#, s, c, marker, cmap, norm, vmin, vmax, alpha, linewidths, faceted, verts, hold)
            cbar = plt.colorbar(CS)
        
        if pp != None: pp.savefig()
    
    # Plot which classifier is used in each zone
    plt.figure()
    
    zmin, zmax = np.unique(Z)[[0,-1]]

    CS = plt.contourf(xx, yy, Z, cmap=cm, levels=np.linspace(start=-0.5, stop=len(SEQUENTIAL_CM)+0.5, num=len(SEQUENTIAL_CM)+1, endpoint=False))
    plt.scatter(X1[correct,0], X1[correct,1],c='g',marker='o',alpha=0.5)#, s, c, marker, cmap, norm, vmin, vmax, alpha, linewidths, faceted, verts, hold)
    plt.scatter(X1[incorrect,0], X1[incorrect,1],c='r',marker='o',alpha=0.5)#, s, c, marker, cmap, norm, vmin, vmax, alpha, linewidths, faceted, verts, hold)
#     cbar = plt.colorbar(CS)
    
    proxy = [plt.Rectangle((0,0),1,1,fc = pc.get_facecolor()[0]) for pc in CS.collections]
    
    ax = plt.gca()
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
    ax.legend( proxy, names[:-2], ncol=4, loc='upper center', bbox_to_anchor=(0.5,-0.1), fontsize='small')
    
    _close_plot(pp)
        
def bar_chart(data, names, labels, x_label, y_label, title, colours=darkcmap, ncol=4, space=None):
    '''
    The space between ticks is always 1, and the widths of the bars are adjusted accordingly.
    
    Parameters
    ----------
    data : np.array [series, points]
        The data that makes up the plot
    '''
    rects = []
    ind = np.arange(data.shape[1])
    space_between = 0.3
    width = (1.0-space_between)/data.shape[0]
    
    # Create the plot
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_title(title)
    ax.set_xticks(ind+(1.0-space_between)/2)
    ax.set_xticklabels( labels )
    
    for i in range(data.shape[0]):
        if space != None and i >= space:
            rects.append( ax.bar(ind+i*width+0.5*width, data[i,:], width, color=colours(i)) )
        else:
            rects.append( ax.bar(ind+i*width, data[i,:], width, color=colours(i)) ) # http://colorbrewer2.org/, alternatively, Set3
    
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
    ax.legend( (r[0] for r in rects), names, ncol=ncol, loc='upper center', bbox_to_anchor=(0.5,-0.1), fontsize='small')
    
    return fig, ax

def create_mesh_grid(X, h=0.001, ctx_features=[0,1]):
    x_min, x_max = X[:, ctx_features[0]].min() - 0.25, X[:, ctx_features[0]].max() + 0.25
    y_min, y_max = X[:, ctx_features[1]].min() - 0.25, X[:, ctx_features[1]].max() + 0.25
#    print x_min, x_max, y_min, y_max
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    
    zz = []
    dims = X.shape[1]
    for i in range(dims):
        if i not in ctx_features:
            zz.append(np.ones(xx.shape) * np.average(X[:,i]))
    
    return xx, yy, zz

def find_mesh_results(clf, xx, yy, zz, ctx_features):
    raveled = [np.c_[xx.ravel(), yy.ravel()]] + [np.c_[z.ravel()] for z in zz]
    Z = clf.predict(np.concatenate(raveled, axis=1))
    Z = Z.reshape(xx.shape)
    
    unique = np.unique(Z)
    
    for i, u in enumerate(unique):
        Z[Z == u] = i
    
    return np.int16(Z)

def space_map(X, y, xx, yy, Z, x_label, y_label, title, ctx_features):
    '''Plots out a map of the context space.
    
    Parameters
    ----------
    X : np.array [samples, context.shape(>=2)]
        The position of the various samples in the context space.
        
    y : np.array [samples]
        The ranking of the correct answer for the sample
        
    xx : np.array [N]
        The horizontal grid for the contour map
    
    yy : np.array [M]
        The vertical grid for the contour map
        
    Z : np.array [N, M]
        The results of context determination for each point on the grid
    '''
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_title(title)
    ax.set_xticks([])
    
    
    # Draw the contoured background
#    print 'SHAPES: ', xx.shape, yy.shape, Z.shape, np.unique(Z), np.sum(Z < 0.5), np.sum(Z > 0.5)
    if len(list(np.unique(Z))) > 1:
#        print list(np.unique(Z)), list(np.unique(Z)+0.5), np.arange(np.min(Z)-0.5, np.max(Z)+1.5, 1.0)
        levels = list(np.unique(Z))#np.arange(np.min(Z)-0.5, np.max(Z)+1.5, 1.0)
#        print 'LEVELS: ', levels
        ax.contourf(xx, yy, Z, cmap=cm.BuGn, levels=levels)
    
    # Draw the points
    #ax.scatter(X[:, ctx_features[0]], X[:, ctx_features[1]], c=y, cmap=cm.RdYlGn)
    
    return fig, ax

def overlayed_space_map(X, y, xx, yy, Z, x_label, y_label, title, ctx_features):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_title(title)
    ax.set_xticks([])
    
    for i, z in enumerate(Z):
#        print xx.shape, yy.shape, z.shape, np.unique(z).shape
        levels = list(np.unique(z))#np.arange(np.min(Z)-0.5, np.max(Z)+1.5, 1.0)
        ax.contourf(xx, yy, z, cmap=SEQUENTIAL_CM[i], levels=levels, alpha=1.0/len(Z))
    
    #ax.scatter(X[:, ctx_features[0]], X[:, ctx_features[1]], c=y, cmap=cm.RdYlGn)
    
    return fig, ax