import sys, os
from sklearn.neighbors.classification import KNeighborsClassifier
sys.path.append(os.path.abspath(os.path.join(__file__,'../../')))

from sklearn.decomposition.pca import RandomizedPCA
from sklearn.externals import joblib
from sklearn import datasets, lda
from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC
from sklearn.metrics.metrics import f1_score
from fruitfusion.feature import HistogramColourFeatures, LocalBinaryPatternTextureFeatures,\
    EdgeOrientationAutocorrelogramFeatures, UnsersDescriptorFeatures,\
    HistogramOfOrientedGradientFeatures
from sklearn.preprocessing import Scaler
from sklearn.pipeline import Pipeline
from scripts.extract_features import get_or_create_destination_dir

RANDOM_STATE = 12
TEST_FRACTION = 2.0/3.0
TRAIN_FRACTION = 1.0/3.0

def evaluate_and_train(features, pipeline, parameters, score_func = f1_score):
    '''Evaluate the various classifier parameters and train one using the best set.
    
    Parameters
    ==========
    features: np.array
        the feature set used to train the classifier
    pipeline: sklearn.pipeline.Pipeline
        the classifier pipeline to train and test
    parameters: list/dict
        a set of parameters to test
    '''
    
    # Split the dataset in three parts, one for training the primary classifier.
    print features.data.shape, features.target.shape
    X_train, X_test, y_train, y_test = train_test_split(
                                            features.data, features.target,
                                            test_size=TEST_FRACTION,
                                            train_size=TRAIN_FRACTION,
                                            random_state=RANDOM_STATE
                                        )
    
    clf = GridSearchCV(pipeline, parameters, score_func=score_func, n_jobs=-2, pre_dispatch='2*n_jobs', verbose=1)
    clf.fit(X_train, y_train, cv=5)

    print "Best parameters set found on development set:"
    print
    print clf.best_estimator_
    print
    print "Grid scores on development set:"
    print
    for params, mean_score, scores in clf.grid_scores_:
        print "%0.3f (+/-%0.03f) for %r" % (
            mean_score, scores.std() / 2, params)
    print

    print "Detailed classification report:"
    print
    print "The model is trained on the full development set."
    print "The scores are computed on the full evaluation set."
    print
    y_true, y_pred = y_test, clf.predict(X_test)
    print classification_report(y_true, y_pred)
    print
    
    print "Retraining classifier:"
    print clf.best_estimator_.get_params()
    best_clf = pipeline.set_params(**clf.best_estimator_.get_params())
    best_clf.fit(X_train, y_train)
    
    return best_clf

def _main(args):
    '''Train a classifier with a variety of parameters and outputs the best one.
    '''
    if args.feature == 'gch':
        features = HistogramColourFeatures.load(args.src)
    elif args.feature == 'lbp':
        features = LocalBinaryPatternTextureFeatures.load(args.src)
    elif args.feature == 'shape':
        features = EdgeOrientationAutocorrelogramFeatures.load(args.src)
    elif args.feature == 'unser':
        features = UnsersDescriptorFeatures.load(args.src)
    elif args.feature == 'hog':
        features = HistogramOfOrientedGradientFeatures.load(args.src)
    else:
        raise ValueError('Invalid feature set %s' %args.feature)
    
    if args.pipeline == 'lda_svc':
        pipeline = Pipeline([('decomp', lda.LDA(n_components=10)),('svc', SVC(C=1.0, probability=True))])
    elif args.pipeline == 'pca_svc':
        pipeline = Pipeline([('decomp', RandomizedPCA(n_components=10, whiten=True)),('svc', SVC(C=1.0, probability=True))])
    elif args.pipeline == 'pca_knn':
        pipeline = Pipeline([('decomp', RandomizedPCA(n_components=10, whiten=True)),('svc', KNeighborsClassifier())])
    else:
        raise ValueError('Invalid pipeline %s' %args.pipeline)

    # Set the parameters by cross-validation
    if args.pipeline.split('_')[1] == 'svc':
        parameters = [{'decomp__n_components':[1, 5, 10, 15, 20, 25, 30, 35, 40, 50, 60, 70, 80, 100], 'svc__kernel': ['rbf'],
                       'svc__gamma': [1e-1, 7.5e-2, 5.5e-2, 5e-2, 4.5e-2, 2.5e-2, 1e-2, 5e-3, 1e-3],
                       'svc__C': [1, 10, 25, 50, 75, 100, 250, 500, 1000]},
    #                        {'decomp__n_components':[10, 25, 40, 50, 60, 70], 'svc__kernel': ['linear'], 'svc__C': [1, 10, 25, 50, 100, 150, 200, 300, 500]}
                            ]
    elif args.pipeline.split('_')[1] == 'knn':
        parameters = [{'decomp__n_components':[10, 20, 25, 30, 35, 40, 50, 60, 70], 'svc__weights':['uniform', 'distance'], 'svc__n_neighbors': [2,4,6,8,12,16,20,32], 'svc__leaf_size':[10,20,30,35,40,45,50,60,80,100] },]
    
    clf = evaluate_and_train(features, pipeline, parameters)
    
    get_or_create_destination_dir(args.out)
    
    joblib.dump(clf, args.out)

if __name__=='__main__':
    '''Example:
    python scripts/evaluate_parameters.py -i ~/documents/school/grad/projects/ai/features/hist_lbp_1/lbp_features.jbl -o ~/documents/school/grad/projects/ai/classifiers/hist_lbp_1/hist_lbp_1.jbl -p pca_svc -f texture
    '''
    
    import argparse
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-i', dest='src', help='Data file')
    parser.add_argument('-o', dest='out', help='Where to save the best classifier')
    parser.add_argument('-p', dest='pipeline', help='What is in the pipeline?')
    parser.add_argument('-f', dest='feature', help='Which feature set to use.')
#    parser.add_argument('-o', dest='dest', help='Output destination')
    
#    subparsers = parser.add_subparsers(help='commands')
#    colour_parser = subparsers.add_parser('lda_svc', help='Classification using an LDA/SVC scheme...')
#    colour_parser.set_defaults(func=_main_lda_svc)
    
    args = parser.parse_args()
#    args.func(args)
    _main(args)
