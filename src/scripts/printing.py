import numpy as np
from sklearn.metrics.metrics import classification_report, zero_one_score,\
    f1_score
from scripts import N_FRUITS, get_rank_of_truth
from fruitfusion.context.multiclass import dict_to_array
from fruitfusion.context.multiclass.locator import correctness

def fused_and_primary(cdf_i, cdf_e, X_test_i, X_test_e, y_test, wv=None):
    f1s = []
    
    print "Detailed classification report:"
    print
    print "The model is trained on the full development set."
    print "The scores are computed on the full evaluation set."
    print
    print "Results for primary classifiers"
    for p in cdf_e.primary:
        y_true, y_pred = y_test[p], cdf_e.primary[p].predict(X_test_e[p])
        print classification_report(y_true, y_pred)
        print 
        f1s.append(f1_score(y_true, y_pred))
        print "Accuracy: ", f1s[-1]
        print
        
    if wv != None:
        print "Results for fused classifiers: Weighted Vote"
        y_true, y_pred = y_test[0], wv.predict(X_test_i)
        print y_true.shape, y_pred.shape
        print classification_report(y_true, y_pred)
        print 
        f1s.append(f1_score(y_true, y_pred))
        print "Accuracy: ", f1s[-1]
        print
        
    print "Results for fused classifiers: Internal"
    y_true, y_pred = y_test[0], cdf_i.predict(X_test_i)
    print y_true.shape, y_pred.shape
    print classification_report(y_true, y_pred)
    print 
    f1s.append(f1_score(y_true, y_pred))
    print "Accuracy: ", f1s[-1]
    print
    
    print "Results for fused classifiers: External"
    y_true, y_pred = y_test[0], cdf_e.predict(X_test_e)
    print y_true.shape, y_pred.shape
    print classification_report(y_true, y_pred)
    print 
    f1s.append(f1_score(y_true, y_pred))
    print "Accuracy: ", f1s[-1]
    print
    
    print ' & '.join(['%.3f' %f for f in f1s])
    
def base_binary(cdf, X_test, y_test):
    print "Base case classification report:"
    print 
    print "The rankings are based on all of the classifiers voting equally"
    print 
    primary = cdf.primary.predict(X_test)
    primary = np.array([primary[p] for p in primary]).T
    
    vote_count = []
    for i in range(cdf.N_CLASSES):
        pos = np.int16(primary == i) # Where the votes are for the class
        
        vote_count.append(np.sum(pos, axis=1))
    vote_count = np.array(vote_count).T
    
    predict = np.argmax(vote_count, axis=1)
    
    print "Results for base case classifier"
    print classification_report(y_test[0], predict)
    
def rankings(cdf, X_test, y_test):
    print "Ranking report:"
    print
    print "Shows the ranking of the correct answer for each classifier."
    print
    print "Results for all classifiers"
    primary_rankings = []
    for p in cdf.primary:
        primary_rankings.append( get_rank_of_truth(cdf.primary_rankings(X_test)[p], y_test[p][:,np.newaxis]) )
    
    truth = y_test[0][:,np.newaxis]
    probs = cdf.predict_proba(X_test)
    order = np.argsort(probs)
    ranking = (order.shape[1] * np.ones(truth.shape)) - np.argwhere(order == truth)[:,1:] - 1
    
    primary_rankings.append(ranking)
    
    hist = np.zeros((N_FRUITS, len(primary_rankings)))
    for i, p in enumerate(primary_rankings):
        hist[:,i] = np.histogram(p, bins=range(N_FRUITS+1))[0]
        
    print hist
    
def context_f1(cdf, X_test, y_test):
    print "Context F1 report:"
    print
    print "Shows the F1 score of the primary classifier in each context zone."
    print
    print "Results for all classifiers"
    
    y_test = y_test[0]
    
    y_ctx = cdf.segmenter.predict(cdf.normalizer.transform(X_test['context']))
    primary = dict_to_array(cdf.primary.predict(X_test)) == y_test[:, np.newaxis]
    
    test_weights = np.zeros((len(cdf.primary), len(cdf.segmenter.locales)))
    for ctx in np.unique(y_ctx):
        test_weights[:, cdf.fuser.locale_index(ctx)] = np.average(primary[y_ctx == ctx], axis=0)
    
    train_test_diff = np.abs(cdf.fuser.weights - test_weights)
    print "Average Difference in F1 Scores Testing/Training:\n", np.average(train_test_diff, axis=1)
    print "Standard Deviation in F1 Scores Testing/Training:\n", np.std(cdf.fuser.weights - test_weights, axis=1)
    
    print
    
def local_f1(cdf, X_test, y_test):
    print "Context F1 report:"
    print
    print "Shows the F1 score of the primary classifier in each context zone."
    print
    print "Results for all classifiers"
    
    y_test = y_test[0]
    
    locale_score_in_training = cdf.locale_scores
    
    locales = cdf.segmenter.predict(cdf.normalizer.transform(X_test['context']))
    
    correct = dict_to_array(correctness({0: X_test}, y_test, classifiers={0: cdf}))
    
    locale_score_in_testing = np.zeros(locale_score_in_training.shape)
    for locale in cdf.locales:
        locale_score_in_testing[locale] = np.average(correct[locales==locale,:])
        
        print 'Locale %s: Training, %s; Testing, %s; Difference, %s;' %(
                str(locale), str(locale_score_in_training[locale]), str(locale_score_in_testing[locale]),
                str(locale_score_in_testing[locale] - locale_score_in_training[locale])
            )
        
    print 
    
def primary_correctness(cdf, X_test, y_test):
    print "Primary Correctness Report"
    print
    print "Shows how often the primary classifiers were correct, and how often they were correct with other classifiers"
    print 
    print "Results for all classifiers"
    print 
    
    y_test = y_test[0]
    
    correct = dict_to_array(correctness(X_test, y_test, cdf.primary))
    n_correct = np.int64(np.sum(correct, axis=1))
    
    # Storing number of times correct
    correct_matrix = np.zeros((len(cdf.primary), len(cdf.primary)))
    for i, p in enumerate(cdf.primary):
        p_is_correct = correct[:,i] == 1
        for j in xrange(len(cdf.primary)):
            correct_matrix[j,i] = np.sum(np.logical_and(p_is_correct, n_correct == j+1))
            
    print correct_matrix
    
    print
    print "The proportion of samples with n correct classifiers"
    print 
    
    proportion = np.float64(np.bincount(n_correct))
    
    print proportion
    
    print 
    print proportion[0]/np.sum(proportion[1:])
    
def selected_correctness(cdf, X_test, y_test):
    print "Selected Correctness Report"
    print
    print "Shows how often the selected classifiers were correct"
    print 
    print "Results for all classifiers"
    print 
    
    y_test = y_test[0]
    
    y_pred = cdf.predict(X_test)
    selected = cdf.fuser.predict_selected
    
    times_selected = np.bincount(selected)
    
    correct_count = np.bincount(selected[y_pred == y_test])
    incorrect_count = np.bincount(selected[y_pred != y_test])
    
    print
    print "Total Times Selected:"
    print np.unique(selected)
    print times_selected
    print
    print "Performance"
    print np.float64(correct_count)/np.float64(times_selected)