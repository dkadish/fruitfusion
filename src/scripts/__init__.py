from sklearn.cross_validation import ShuffleSplit
import numpy as np

N_FRUITS = 15

def get_secondary_train_test(features, train_frac, random_state):
    #FIXME: this NEEDS to be validated
    
    cv = ShuffleSplit(features[0].data.shape[0], 
                  train_size=train_frac,
                  test_size=train_frac,
                  random_state=random_state,
                  indices=True)
    cv_iter = iter(cv)
    
    # Iterate past the data used for training the primary classifiers
    tr1, te1 = next(cv_iter)
    train, test = next(cv_iter)
    
    assert not (train == tr1).all() and not (test == te1).all(), 'The two sets should not be equal.'
    
    return train, test

def get_xy_train_test(features, train, test):
    tt = map(lambda (k,f): [(k, t) for t in (f.data[train], f.data[test], f.target[train], f.target[test])], features.iteritems())
    X_train, X_test, y_train, y_test = [dict(z) for z in zip(*tt)]
    
    return X_train, X_test, y_train, y_test

def get_rank_of_truth(ranking, truth):
    order = np.argsort(ranking)
    truth_ranking = (order.shape[1] * np.ones(truth.shape)) - np.argwhere(order == truth)[:,1:] - 1 # Zero-indexed
    
    return truth_ranking