import sys, os
from sklearn.decomposition.pca import RandomizedPCA
from scripts.visualization import darkcmap_contours
sys.path.append(os.path.abspath(os.path.join(__file__,'../../')))

from sklearn.externals import joblib
import numpy as np

# Imports from my code
from fruitfusion.feature import ImageFeatures
from fruitfusion.context.multiclass.cdf import KNeighbourSupportVectorWeightedVotingCDF,\
    RNeighbourSupportVectorWeightedVotingCDF,\
    WRNeighbourSupportVectorWeightedVotingCDF, DCSLA, GaussianKDELocalAccuracy,\
    KdeSvmSelectOrVoteCDF, WeightedVote, KNeighbourSvmSelectOrVote,\
    KNeighbourSelectOrVote, KdeDTreeSelectOrVoteCDF,\
    RNeighbourSvmSelectOrVoteCDF

from scripts import get_secondary_train_test, get_xy_train_test, printing,\
    visualization
from scripts.evaluate_parameters import TRAIN_FRACTION, RANDOM_STATE

def _main(args):
    features = dict(enumerate(map(lambda f: ImageFeatures.load(f), args.feature)))
    if args.feature_test == None:
        features_test = features
    else:
        features_test = dict(enumerate(map(lambda f: ImageFeatures.load(f), args.feature_test)))
    classifiers = dict(enumerate(map(joblib.load, args.classifier)))
    
    # External Context
    ctx_ext = joblib.load(args.context)
    if args.context_test == None:
        ctx_ext_test = ImageFeatures('', target_names=None)
        ctx_ext_test._data = ctx_ext._data.copy()
    else:
        ctx_ext_test = joblib.load(args.context_test)

    # Internal Context
    d = []
    ctx_int = ImageFeatures('', target_names=None)
    for f in features.itervalues():
        if ctx_int.target_names == None: ctx_int._target_names = f.target_names
        d.append(f._data)
    ctx_int._data = np.concatenate(d)
    
    ctx_int_test = ImageFeatures('', target_names=None)
    d = []
    for f in features_test.itervalues():
        if ctx_int_test.target_names == None: ctx_int._target_names = f.target_names
        d.append(f._data)
    ctx_int_test._data = np.concatenate(d)
    
    # Which features to use
    FEATS = [2,6,7,13,14,15,16,17,18,19]
    ctx_ext._data = ctx_ext.data[:,FEATS].T
    ctx_ext_test._data = ctx_ext_test.data[:,FEATS].T
    
    ctx_int._data = ctx_int.data.T
    ctx_int_test._data = ctx_int_test.data.T
    

#    get_results(features, classifiers, ctx, context_classifier=args.context_classifier, output=args.output)
    
    train, test = get_secondary_train_test(features, TRAIN_FRACTION, RANDOM_STATE)
    
    X_train, a, y_train, b = get_xy_train_test(features, train, test)
    del a,b
    a, X_test, b, y_test = get_xy_train_test(features_test, train, test)
    del a,b
    
    assert reduce(lambda t, (i, y): (y_train.values()[i] == y_train.values()[i+1]).all() and t, enumerate(y_train.values()[:-1]), True), 'Testing truths must be the same.'
    
    
    names_4 = ('GCH', 'LBP', 'Unser', 'HOG', 'DCS-LA (I)', 'DCS-LA (E)')
    names_8 = (
        'GCH (I)', 'GCH (E)', 'LBP (I)', 'LBP (E)',
        'Unser (I)', 'Unser (E)', 'HOG (I)', 'HOG (E)',
        'DCS-LA (I)', 'DCS-LA (E)')
    names_int_ext_vote = (
        'GCH (I)', 'GCH (E)', 'LBP (I)', 'LBP (E)',
        'Unser (I)', 'Unser (E)', 'HOG (I)', 'HOG (E)',
        'PWV (I)', 'PWV (E)',
        'WV (I)', 'WV (E)'
    )
    
    if args.context_classifier == 'knn-svm-weighted':
        cdf_ext = KNeighbourSupportVectorWeightedVotingCDF(classifiers)
        cdf_int = KNeighbourSupportVectorWeightedVotingCDF(classifiers)
    elif args.context_classifier == 'rnn-svm-weighted':
        cdf_ext = RNeighbourSupportVectorWeightedVotingCDF(classifiers)
        cdf_int = RNeighbourSupportVectorWeightedVotingCDF(classifiers)
    elif args.context_classifier == 'wrnn-svm-weighted':
        cdf_ext = WRNeighbourSupportVectorWeightedVotingCDF(classifiers)
        cdf_int = WRNeighbourSupportVectorWeightedVotingCDF(classifiers)
    elif args.context_classifier == 'dcsla':
        k = 15 #20 # O: 78%, M:82%
        cdf_ext = DCSLA(classifiers, k=k)
        cdf_int = DCSLA(classifiers, k=k)
    elif args.context_classifier == 'gkla':
        cdf_ext = GaussianKDELocalAccuracy(classifiers)
        cdf_int = GaussianKDELocalAccuracy(classifiers)
    elif args.context_classifier == 'kde':
        thresh, factor, C, gamma = [0.85], 0.5, 2, 1 #0.98, None, 0.1, 0.1 # 88%, 86 avg, 86 int
#         thresh, factor, C, gamma = 0.80, 0.5, 1, 1
        cdf_ext = KdeSvmSelectOrVoteCDF(classifiers, thresh=thresh, factor=factor, C=C, gamma=gamma)
#         cdf_int = WeightedVote(classifiers)
        cdf_int = KdeSvmSelectOrVoteCDF(classifiers, thresh=thresh, factor=factor, C=C, gamma=gamma)
    elif args.context_classifier == 'knn-svm-sov':
#         thresh, factor, C, gamma = 0.98, None, 1, 1 # 87.4%
        n, k, C, gamma = 4, 25, 2, None #4, 25, 30, None #2, 35, 30, None #None, 16, 20, None
        cdf_ext = KNeighbourSvmSelectOrVote(classifiers, n=n, k=k, C=C, gamma=gamma)
#         cdf_int = WeightedVote(classifiers)
        cdf_int = KNeighbourSvmSelectOrVote(classifiers, n=n, k=k, C=C, gamma=gamma)
    elif args.context_classifier == 'kde-dtree-sov':
#         thresh, factor, C, gamma = 0.98, None, 1, 1 # 87.4%
        thresh, factor, criterion, max_depth = [0.9], 0.375, None, 15
        cdf_ext = KdeDTreeSelectOrVoteCDF(classifiers, thresh=thresh, factor=factor, criterion=criterion, max_depth=max_depth)
#         cdf_int = WeightedVote(classifiers)
        cdf_int = KdeDTreeSelectOrVoteCDF(classifiers, thresh=thresh, factor=factor, criterion=criterion, max_depth=max_depth)
    elif args.context_classifier == 'knn-sov':
        names_4 = ('GCH', 'LBP', 'Unser', 'HOG', 'GWV', 'CDF-NA (I)', 'CDF-NA (E)')
        k, thresh = 100, 0.9
        if args.modified_dataset:
            k, thresh = 65, 0.85
        print 'Params: ', k, thresh
        cdf_ext = KNeighbourSelectOrVote(classifiers, k=k, thresh=thresh)
#         cdf_int = WeightedVote(classifiers)
        cdf_int = KNeighbourSelectOrVote(classifiers, k=k, thresh=thresh)
    elif args.context_classifier == 'wrnn-svm-sov': 
        r, thresh, C, gamma = 0.4, [0.85], 2, 1 
        cdf_ext = RNeighbourSvmSelectOrVoteCDF(classifiers, r=r, thresh=thresh, C=C, gamma=gamma)
#         cdf_int = WeightedVote(classifiers)
        cdf_int = RNeighbourSvmSelectOrVoteCDF(classifiers, r=r, thresh=thresh, C=C, gamma=gamma)
    
    # Set up the context features and predictions
    X_ctx_ext = ctx_ext.data[train,:]
    pca = RandomizedPCA(n_components=100)
    X_ctx_int = pca.fit_transform(ctx_int.data[train,:])
    
    print 'INTERNAL'
    X_train['context'] = X_ctx_int
    cdf_int.fit(X_train, y_train[y_train.keys()[0]])
    
    # Do weighted vote, if necessary
    if args.weighted_vote:
        print 'WEIGHTED'
        wv = WeightedVote(classifiers)
        wv.fit(X_train, y_train[y_train.keys()[0]])
    else:
        wv=None
    
    print 'EXTERNAL'
    # Fit the contextual classifier
    X_train['context'] = X_ctx_ext
    cdf_ext.fit(X_train, y_train[y_train.keys()[0]])
    
    # Deal with the testing
    X_context_ext = ctx_ext_test.data[test,:]
    X_test_ext = X_test.copy()
    X_test_ext['context'] = X_context_ext
    X_context_int = pca.transform(ctx_int_test.data[test,:])
    X_test_int = X_test.copy()
    X_test_int['context'] = X_context_int
    
    printing.fused_and_primary(cdf_int, cdf_ext, X_test_int, X_test_ext, y_test, wv)
    if args.context_classifier == 'ranking':    
        printing.rankings(cdf_ext, X_test_ext, y_test)
    elif args.context_classifier.split('-')[0] == 'binary':
        printing.base_binary(cdf_ext, X_test_ext, y_test)
    elif args.context_classifier == 'dcsla' or args.context_classifier == 'gkla':
        printing.selected_correctness(cdf_ext, X_test_ext, y_test)
#     printing.context_f1(cdf_ext, X_test_ext, y_test)
    if args.context_classifier != 'dcsla' and args.context_classifier != 'knn-sov':
        printing.local_f1(cdf_ext, X_test_ext, y_test)
    printing.primary_correctness(cdf_ext, X_test_ext, y_test)
    
    visualization.plot_compared_scores(cdf_int, cdf_ext, X_test_int, X_test_ext, y_test, names_4, wv)
#     visualization.plot_compared_base_scores(cdf, X_test, y_test)
#     visualization.plot_compared_all(cdf, X_test, y_test, names)
#     visualization.plot_rankings(cdf, X_test, y_test, names)
#     if args.context_classifier == 'ranking':    
#         visualization.map_zones_ranked(cdf, X_test, y_test, names)
#     elif args.context_classifier.split('-')[0] == 'binary':   
#         visualization.map_zones_binary(cdf, X_test, y_test, names)
    if args.context_classifier == 'dcsla':
        visualization.plot_n_selections(cdf_int, cdf_ext, X_test_int, X_test_ext, y_test, names_8)
        visualization.plot_selected_performance(cdf_int, cdf_ext, X_test_int, X_test_ext, y_test, names_8)
        visualization.map_cdf_selected_zones(cdf_ext, X_test_ext, y_test, names_4, ctx_features=[2,3], t='ext3')
        visualization.map_cdf_selected_zones(cdf_ext, X_test_ext, y_test, names_4, ctx_features=[2,6], t='ext6')
        visualization.map_cdf_selected_zones(cdf_ext, X_test_ext, y_test, names_4, ctx_features=[2,8], t='ext8')
        visualization.map_cdf_selected_zones(cdf_int, X_test_int, y_test, names_4, ctx_features=[2,3], t='int')
    if args.context_classifier == 'knn-sov':
        # distribution of selected elements between the primary classifiers distribution of samples among 3 types of classification
        visualization.plot_fusion_type(cdf_int, cdf_ext, X_test_int, X_test_ext, y_test, names=names_int_ext_vote)
        visualization.map_cdf_selected_zones(cdf_ext, X_test_ext, y_test, names_4, ctx_features=[2,3], t='ext3', plot_primary=False, cm=darkcmap_contours)
#         visualization.map_cdf_selected_zones(cdf_ext, X_test_ext, y_test, names_4, ctx_features=[2,6], t='ext6', plot_primary=False)
#         visualization.map_cdf_selected_zones(cdf_ext, X_test_ext, y_test, names_4, ctx_features=[2,8], t='ext8', plot_primary=False)
        # map of the type of fusion done...
        
        pass
    visualization.plot_n_correct_classifiers(cdf_ext, X_test_ext, y_test)

if __name__=='__main__':
    '''Example:
    python ~/documents/workspace/fruitfusion/src/scripts/multiclass.py \
        -f features/hist_colour_2/colour_features.jbl features/hist_lbp_1/lbp_features.jbl \
        -c classifiers/hist_colour_2/hist_colour_2.jbl classifiers/hist_lbp_1/hist_lbp_1.jbl
        -x features/ \
    '''
    
    import argparse
    
    parser = argparse.ArgumentParser()
    
#    parser.add_argument('-o', dest='out', help='Where to save the best classifier')
#    parser.add_argument('-p', dest='pipeline', help='What is in the pipeline?')
    parser.add_argument('-f', dest='feature', nargs='+', help='Which feature set to use.')
    parser.add_argument('--f-test', dest='feature_test', nargs='+', help='Features with noise', default=None)
    parser.add_argument('-c', dest='classifier', nargs='+', help='Data file')
    parser.add_argument('-x', dest='context', help='Context', default=None)
    parser.add_argument('--x-test', dest='context_test', help='Context with noise', default=None)
#    parser.add_argument('-o', dest='output', help='Output destination')
    parser.add_argument('-t', dest='context_classifier', help='Context classifier type')
    parser.add_argument('-w', dest='weighted_vote', action='store_true', help='Process for the weighted vote classifier')
    parser.add_argument('-m', dest='modified_dataset', action='store_true', help='Using the modified dataset')
    
#    subparsers = parser.add_subparsers(help='commands')
#    colour_parser = subparsers.add_parser('lda_svc', help='Classification using an LDA/SVC scheme...')
#    colour_parser.set_defaults(func=_main_lda_svc)
    
    args = parser.parse_args()
#    args.func(args)
    _main(args)