import sys, os
sys.path.append(os.path.abspath(os.path.join(__file__,'../../')))

from sklearn.decomposition.kernel_pca import KernelPCA
from sklearn.pipeline import Pipeline
from sklearn.neighbors.classification import KNeighborsClassifier
from sklearn.preprocessing import Scaler
from sklearn.decomposition.pca import RandomizedPCA
from sklearn.lda import LDA
import scipy
from context import NearestNeighboursCDF, SupportVectorCDF, RadiusNeighboursCDF

from argparse import ArgumentError
from scripts import get_secondary_train_test, get_xy_train_test
from sklearn.externals import joblib
from feature import ImageFeatures
from sklearn.cross_validation import train_test_split, ShuffleSplit
from sklearn.metrics.metrics import classification_report, zero_one_score
from sklearn.feature_selection import SelectKBest, chi2, f_classif
from scripts.evaluate_parameters import RANDOM_STATE, TRAIN_FRACTION
import numpy as np

TAKE = [0,7]
BINS = 30
N_NEIGHBOURS = 9
WEIGHTS = 'uniform'

def separation(X,y):
    '''Data separation
    
    Find histogram along the axis. Measure covariance between class histograms.`
    
    Parameters
    ==========
    X : np.ndarray
        Feature vector containing all possible contextual features
        
    y : np.ndarray
        Truth vector containing the number of the correct classifier
        (-1 if both are right, -2 if none are correct)
    '''
    # Taking features only where y is not 'both' or 'neither'
    X_cls = [X[y==y_cls,:] for y_cls in set(y[y>=0])]
    correl = []
    for i in xrange(0,X_cls[0].shape[1]):
        hist, bins = zip(*[np.histogram(x[:,i], bins=BINS, range=(X[:,i].min(), X[:,i].max())) for x in X_cls])
        assert len(hist) == 2
        correl.append( scipy.stats.pearsonr(hist[0], hist[1])[0] )# Get the correlation between the histograms for both classifiers
        
    ind = [np.argmax(correl==i) for i in sorted(correl)]
        
    print correl, ind
    
    return ind

def plot_context(cdf, X_ctx, ind, names, results):
    # Plot the context
    import matplotlib
    matplotlib.use('GTK')
    import matplotlib.pyplot as plt
    
    c_mesh = np.array([
            [0.5, 0.7, 1.0],
            [0.85, 1.0, 0.45],
        ])
    c_markers = np.array([
            [0.09, 0.46, 1.0],
            [0.73, 1.0, 0.0],
            [1.0, 0.0, 0.7],
        ])
    cm = matplotlib.colors.ListedColormap(c_mesh, name='half_rg')
    
    # Plot the NN Graph
    h = 0.02
    x_min, x_max = X_ctx[:, ind[0]].min() - 1, X_ctx[:, ind[0]].max() + 1
    y_min, y_max = X_ctx[:, ind[1]].min() - 1, X_ctx[:, ind[1]].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                 np.arange(y_min, y_max, h))
    Z = cdf.context.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    
    fig = plt.figure()
    ax = plt.axes()
    ax.set_color_cycle(c_markers)
    plt.xlabel(names[ind[0]])
    plt.ylabel(names[ind[1]])
#    ax.set_xscale('log')
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    plt.pcolormesh(xx, yy, Z, cmap=cm)
#FIXME    ax.plot(X_ctx[results['correct'],ind[0]], X_ctx[results['correct'],ind[1]], 'o')
    for f in cdf.primary:
        plt.plot(X_ctx[results['which'][f],ind[0]], X_ctx[results['which'][f],ind[1]], 'o')
    plt.plot(X_ctx[results['neither'],ind[0]], X_ctx[results['neither'],ind[1]], '+')
    plt.show()
    
    return fig
        
def get_results(features, classifiers, context, context_classifier, output=None):
    
    train, test = get_secondary_train_test(features, TRAIN_FRACTION, RANDOM_STATE)
    
    X_train, X_test, y_train, y_test = get_xy_train_test(features, train, test)
    
    assert reduce(lambda t, (i, y): (y_train.values()[i] == y_train.values()[i+1]).all() and t, enumerate(y_train.values()[:-1]), True), 'Testing truths must be the same.'
    
    if context_classifier == 'nn':
        cdf = NearestNeighboursCDF(classifiers, n_neighbors=N_NEIGHBOURS, weights=WEIGHTS, leaf_size=10)
    elif context_classifier == 'svc':
        cdf = SupportVectorCDF(classifiers, C=5)
    elif context_classifier == 'rad':
        cdf = RadiusNeighboursCDF(classifiers, radius=0.75, weights='distance', algorithm='auto', leaf_size=20, outlier_label=1)
    else:
        raise ArgumentError('Invalid context classifier argument')
    y_ctx = cdf.context_truth(cdf.primary_predictions(X_train), y_train[0])
    X_ctx = context.data[train,:]
    
    # Feature Selection
    print separation(X_ctx, y_ctx)
    TAKE = separation(X_ctx, y_ctx)
    print TAKE
    TAKE = TAKE[:2]
    X_ctx = np.take(X_ctx, TAKE, axis=1)
    
    # Data preprocessing
    scale = Scaler()
    X_ctx = scale.fit_transform(X_ctx, y_ctx)
    
    X_train['context'] = X_ctx
    
    cdf.fit(X_train, y_train[y_train.keys()[0]])
    
    ind = [0,1]
    names = ['sat-val level', 'grey-level  entropy']
    figs = []
    figs.append( plot_context(cdf, X_ctx, ind, names, cdf.primary_results) )
    
#    print 'C1'
#    print classification_report(y_test[0], y_pred[0])
#    print 'C2'
#    print classification_report(y_test[1], y_pred[1])

    # Evaluate the performance of the classifier
    print "Detailed classification report:"
    print
    print "The model is trained on the full development set."
    print "The scores are computed on the full evaluation set."
    print
    print "Results for primary classifiers"
    for p in cdf.primary:
        print X_test[p].shape, p
        y_true, y_pred = y_test[p], cdf.primary[p].predict(X_test[p])
        print classification_report(y_true, y_pred)
        print 
        print "Accuracy: ", zero_one_score(y_true, y_pred)
        print
    print "Results for fused classifiers"
    X_context = np.take(context.data[test,:], TAKE, axis=1)
    X_context = scale.transform(X_context)
    X_test['context'] = X_context
    y_true, y_pred = y_test[cdf.primary.keys()[0]], cdf.predict(X_test)
    print classification_report(y_true, y_pred)
    print
    print 'Results for class selection'
    ctx_truth = cdf.context_truth(cdf.primary_predictions(X_test), y_test[0])
    print classification_report(ctx_truth[ctx_truth>=0], cdf.context.predict(X_context[ctx_truth>=0,:]))
    print 
    print "Accuracy: ", zero_one_score(ctx_truth[ctx_truth>=0], cdf.context.predict(X_context[ctx_truth>=0,:]))
    print
    
    # Graph the results of the classification
    figs.append( plot_context(cdf, X_context, ind, names, cdf.primary_results) )
    
    if output != None:
        from matplotlib.backends.backend_pdf import PdfPages
        pp = PdfPages(output)
        for f in figs:
            print f
            f.savefig(pp, format='pdf')
        pp.close()

def _main_compare(args):
    features = dict(enumerate(map(lambda f: ImageFeatures.load(f), args.feature)))
    classifiers = dict(enumerate(map(joblib.load, args.classifier)))
    ctx = joblib.load(args.context)
    get_results(features, classifiers, ctx, context_classifier=args.context_classifier, output=args.output)

if __name__=='__main__':
    '''Example:
    python ~/documents/workspace/fruitfusion/src/scripts/compare_classifiers.py -f features/hist_colour_2//colour_features.jbl features/hist_lbp_1/lbp_features.jbl -c classifiers/hist_colour_2/hist_colour_2.jbl classifiers/hist_lbp_1/hist_lbp_1.jbl
    '''
    
    import argparse
    
    parser = argparse.ArgumentParser()
    
#    parser.add_argument('-o', dest='out', help='Where to save the best classifier')
#    parser.add_argument('-p', dest='pipeline', help='What is in the pipeline?')
    parser.add_argument('-f', dest='feature', nargs='+', help='Which feature set to use.')
    parser.add_argument('-c', dest='classifier', nargs='+', help='Data file')
    parser.add_argument('-x', dest='context', help='Context')
    parser.add_argument('-o', dest='output', help='Output destination')
    parser.add_argument('-t', dest='context_classifier', help='Context classifier type')
    
#    subparsers = parser.add_subparsers(help='commands')
#    colour_parser = subparsers.add_parser('lda_svc', help='Classification using an LDA/SVC scheme...')
#    colour_parser.set_defaults(func=_main_lda_svc)
    
    args = parser.parse_args()
#    args.func(args)
    _main_compare(args)
