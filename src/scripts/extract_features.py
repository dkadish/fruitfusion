import sys, os
sys.path.append(os.path.abspath(os.path.join(__file__,'../../')))

from fruitfusion.feature.context import NoiseFeatures, ColourFeatures, ContextFeatures
from fruitfusion.feature import HistogramColourFeatures, LocalBinaryPatternTextureFeatures,\
    EdgeOrientationAutocorrelogramFeatures,\
    UnsersDescriptorFeatures, UnserContextFeatures, UnserColourContextFeatures,\
    HistogramOfOrientedGradientFeatures

def load_files_and_targets(directory):
        # Generate list of files
    load_pattern = []
    targets = []
    for root,dir,files in os.walk(directory):
        for f in filter(lambda f: os.path.splitext(f)[-1] == '.jpg', files):
            load_pattern.append(os.path.join(root,f))
            targets.append(os.path.split(root)[-1])
            
    return load_pattern, targets

def get_or_create_destination_dir(destination):
    directory = os.path.split(destination)[0]
    if not os.path.exists(directory):
        os.mkdir(directory)
        
    assert os.path.exists(directory), 'Directory %s should exist by now...' %directory
    
    return True

def _main_context(args):
    feature_class = ContextFeatures
    __main(args, feature_class)

def _main_gch(args):
    feature_class = HistogramColourFeatures
    __main(args, feature_class)

def _main_texture(args):
    feature_class = LocalBinaryPatternTextureFeatures
    __main(args, feature_class)

def _main_eoac(args):
    feature_class = EdgeOrientationAutocorrelogramFeatures
    __main(args, feature_class)

def _main_unser(args):
    feature_class = UnsersDescriptorFeatures
    __main(args, feature_class)

def _main_unser_context(args):
    feature_class = UnserContextFeatures
    __main(args, feature_class)

def _main_unser_colour_context(args):
    feature_class = UnserColourContextFeatures
    __main(args, feature_class)

def _main_hog(args):
    feature_class = HistogramOfOrientedGradientFeatures
    __main(args, feature_class)
    
def _main_noise(args):
    feature_class = NoiseFeatures
    __main(args, feature_class)
    
def _main_colour(args):
    feature_class = ColourFeatures
    __main(args, feature_class)
    
def __main(args, feature_class):
    get_or_create_destination_dir(args.dest)
    load_pattern, targets = load_files_and_targets(args.dir)
            
    features = feature_class(load_pattern, targets)
    features.save(args.dest)
    
if __name__=='__main__':
    '''Example:
    python scripts/extract_features.py -i ~/documents/school/grad/projects/ai/tropical-fruits/ -o ~/documents/school/grad/projects/ai/features/hist_colour_2/colour_features.jbl colour
    '''
    
    import argparse
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-i', dest='dir', help='Main image directory')
    parser.add_argument('-o', dest='dest', help='Output destination')
#    parser.add_argument('-t', dest='type', help='Feature type')
    
    subparsers = parser.add_subparsers(help='commands')
    colour_parser = subparsers.add_parser('gch', help='Extracts colour based features...')
    colour_parser.set_defaults(func=_main_gch)
    
    context_parser = subparsers.add_parser('context', help='Extracts contextual features...')
    context_parser.set_defaults(func=_main_context)
    
    texture_parser = subparsers.add_parser('lbp', help='Extracts texture based features...')
    texture_parser.set_defaults(func=_main_texture)
    
    texture_parser = subparsers.add_parser('eoac', help='Extracts EOAC based features...')
    texture_parser.set_defaults(func=_main_eoac)
    
    texture_parser = subparsers.add_parser('unser', help='Extracts Unser Descriptor based features...')
    texture_parser.set_defaults(func=_main_unser)
    
    texture_parser = subparsers.add_parser('unser_context', help='Extracts Unser Descriptor based contextual features...')
    texture_parser.set_defaults(func=_main_unser_context)
    
    texture_parser = subparsers.add_parser('unser_colour_context', help='Extracts Unser/Colour Descriptor based contextual features...')
    texture_parser.set_defaults(func=_main_unser_colour_context)
    
    texture_parser = subparsers.add_parser('hog', help='Extracts HOG features...')
    texture_parser.set_defaults(func=_main_hog)
    
    texture_parser = subparsers.add_parser('noise', help='Extracts noise features...')
    texture_parser.set_defaults(func=_main_noise)
    
    texture_parser = subparsers.add_parser('colour', help='Extracts colour context features...')
    texture_parser.set_defaults(func=_main_colour)
    
    args = parser.parse_args()
    args.func(args)
